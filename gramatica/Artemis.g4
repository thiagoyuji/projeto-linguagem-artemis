grammar Artemis;

/* 
    Regra Inicial - Start -> 
*/ 
artemis
    : importRule
      struct*
      globalVariable*
      functions*
      mainFunction
      EOF;

/* 
    IMPORT ->
        * call's/ import other library 
        * io library is required

    EXAMPLES -> 
        import io_library;
        import file;
    
    STATUS ->
        OK - WORKING
*/
importRule: externImport*;

externImport: IMPORT IDENTIFIER LINE_DELIMITER;

/* 
    STRUCT -> 
        * abstract data type

    EXAMPLES ->
        struct Pessoa {
            variable nome : string;
            variable idade : int;
        };

    STATUS ->

        OK - WORKING
*/
struct
    : STRUCT STRUCT_IDENTIFIER 
      '{' variableDeclaration+ '}'
      LINE_DELIMITER;

/* 
    MAIN FUNCTION ->
        * init function
        * this function is required
    
    EXAMPLES ->
        function main() : void {
            //instructions
        }
    
    STATUS ->
        OK - WORKING
*/
mainFunction: FUNCTION IDENTIFIER '()' '{' escope* '}';

/* 
    FUNCTIONS -> 
        * create functions
        * call functions

    EXAMPLES ->
        function somar( num1 : int, num2 : int ) : int{
            return num1 + num2;
        }

    STATUS ->
        OK - WORKING
*/
functions
    : FUNCTION IDENTIFIER '(' parametersDefinition ')'
      (TYPE_DELIMITER types)?
      '{' escope* returnRule? '}';

functionCall
    : IDENTIFIER 
      ('(' parametersPassing ')' | '()')
      LINE_DELIMITER?;

parametersDefinition
    : (IDENTIFIER TYPE_DELIMITER types)
      ( ',' IDENTIFIER TYPE_DELIMITER types)*;

parametersPassing: values(',' values)*;

/* 
    RETURN -> 
        * return some value

    EXAMPLES ->
        return 10;

    STATUS ->
        OK - WORKING
*/
returnRule: RETURN values LINE_DELIMITER;

/* 
    ESCOPE ->
        * all in {}

    STATUS -> 
        OK - WORKING
*/
escope
    : localVariable
    | variableAssigner
    | structFieldAssigner
    | functionCall
    | conditionals
    | whileLoop;

/* 
    CONDITIONALS ->
        * flux control

    EXAMPLES ->
        if( variable == "ola" ){
            print(variable);
        }else{
            return variable;
        }

    STATUS ->
        OK - WORKING
*/
conditionals: ifRule elseRule?;

ifRule
    : IF '(' expression ')'
      '{' escope* returnRule? '}'
      elseIfRule?;

elseIfRule: ELSE ifRule;

elseRule: ELSE '{' escope* returnRule? '}';

/* 
    LOOPS ->
        * repeat escope of instructions
    
    EXAMPLES -> 
        variable contador : int <- 0;

        while( contador < 10 ){
            print(contador);
            contador++;
        }

    STATUS ->
        OK - WORKING
*/
whileLoop
    : WHILE '(' expression ')'
      '{' escope* '}';

/* 
    EXPRESSIONS ->
        * arithmetic expression
        * logic expression
        * relational expression
        * separated by type

    EXAMPLES ->
        + 2 + 2 + 4 - 4 * 3 / 4 % 2
        2 = 2
        true && true

    STATUS ->
        OK - WORKING
*/
expression
    : intExpression 
    | floatExpression 
    | charExpression 
    | stringExpression 
    | booleanExpression;

intExpression
    : (intValues multiplicationOperators sumOperators* intExpression)
    | (intValues sumOperators+ intExpression)
    | (intValues logicOperators intExpression)
    | (intValues relationalOperators intExpression)
    | ('(' intExpression ')' (allOperators intExpression)?)
    | (intValues);

floatExpression
    : (floatValues multiplicationOperators sumOperators* floatExpression)
    | (floatValues sumOperators+ floatExpression)
    | (floatValues logicOperators floatExpression)
    | (floatValues relationalOperators floatExpression)
    | ('(' floatExpression ')' (allOperators floatExpression)?)
    | (floatValues);

charExpression
    : (charValues logicOperators charExpression)
    | (charValues relationalOperators charExpression)
    | ('(' charExpression ')' (logicRelationaOperators charExpression)?)
    | charValues;

stringExpression
    : (stringValues logicOperators stringExpression)
    | (stringValues relationalOperators stringExpression)
    | ('(' stringExpression ')' (logicRelationaOperators stringExpression)?)
    | stringValues;

booleanExpression
    : (booleanValuesExpression logicOperators booleanExpression)
    | (booleanValuesExpression relationalOperators booleanExpression)
    | ('(' booleanExpression ')' (logicRelationaOperators booleanExpression)?)
    | booleanValuesExpression;

intValues
    : sumOperators* NUMBERS
    | functionCall
    | IDENTIFIER;

floatValues
    : sumOperators* NUMBERS
    | sumOperators* FLOATING_POINT
    | functionCall
    | IDENTIFIER;

charValues
    : CHARACTER
    | IDENTIFIER
    | functionCall;

stringValues
    : LITERAL_STRING
    | IDENTIFIER
    | functionCall;

booleanValuesExpression
    : booleanValues
    | IDENTIFIER
    | functionCall;

allOperators
    : sumOperators
    | multiplicationOperators
    | logicOperators
    | relationalOperators;

logicRelationaOperators
    : logicOperators
    | relationalOperators;

sumOperators
    : ADD 
    | SUB;

multiplicationOperators
    : MULT 
    | DIV 
    | REST;

logicOperators
    : AND 
    | OR;

relationalOperators
    : EQUAL 
    | DIFFERENT 
    | BIGGER_THAN 
    | BIGGER_EQUAL_THAN
    | LESS_THAN
    | LESS_EQUAL_THAN;

/* 
    VARIABLES -> 
        * local variables
        * global variables

    EXAMPLES ->
        global variable nome : string <- "meuNome";
        variable idade : int <- 10;

    STATUS ->
        OK - WORKING 
*/
globalVariable: GLOBAL IDENTIFIER TYPE_DELIMITER typeAssignerValue LINE_DELIMITER;

localVariable: VARIABLE IDENTIFIER TYPE_DELIMITER typeAssignerValue LINE_DELIMITER;

variableDeclaration: VARIABLE IDENTIFIER TYPE_DELIMITER types LINE_DELIMITER;   

variableAssigner: IDENTIFIER ASSIGNER values LINE_DELIMITER;

structFieldAssigner: IDENTIFIER '.' IDENTIFIER ASSIGNER values LINE_DELIMITER;

typeAssignerValue
    : (INT ASSIGNER (NUMBERS | intExpression))
    | (FLOAT ASSIGNER (FLOATING_POINT | floatExpression))
    | (CHAR ASSIGNER CHARACTER)
    | (STRING ASSIGNER LITERAL_STRING)
    | (BOOLEAN ASSIGNER (booleanValues | booleanExpression))
    | (STRUCT_IDENTIFIER ASSIGNER IDENTIFIER)
    | IDENTIFIER
    | functionCall;

/*
    COMMON ->
        * common for all grammar

    STATUS ->
        OK - WORKING
*/
types
    : INT
    | FLOAT
    | CHAR
    | STRING
    | BOOLEAN
    | STRUCT_IDENTIFIER;

values
    : NUMBERS
    | FLOATING_POINT
    | CHARACTER
    | LITERAL_STRING
    | TRUE
    | FALSE
    | IDENTIFIER
    | functionCall
    | expression;

booleanValues: TRUE | FALSE;

/* 
    TOKENS -> 
*/
IMPORT : 'import';
GLOBAL : 'global';
VARIABLE : 'variable';
FUNCTION : 'function';
RETURN : 'return';
IF : 'if';
ELSE : 'else';
WHILE : 'while';

INT : 'int';
FLOAT : 'float';
CHAR : 'char';
STRING : 'string';
BOOLEAN : 'boolean';
STRUCT : 'struct';
TRUE : 'true';
FALSE : 'false';

ADD : '+';
SUB : '-';
MULT : '*';
DIV : '/';
REST : '%';
EQUAL : '=';
DIFFERENT : '!=';
BIGGER_THAN : '>';
LESS_THAN : '<';
BIGGER_EQUAL_THAN : '>=';
LESS_EQUAL_THAN : '<=';
AND : '&&';
OR : '||';

ASSIGNER : '<-';
TYPE_DELIMITER : ':';
LINE_DELIMITER : ';';

NUMBERS : [0-9]+;

FLOATING_POINT : [0-9]+'.'[0-9]+;

CHARACTER : '\''[a-zA-Z]'\'';

LITERAL_STRING : '"'[a-zA-Z0-9_.,:;\\"*#$%@!><=/?| ]+'"';

STRUCT_IDENTIFIER : [A-Z][a-zA-Z0-9_]*; 

IDENTIFIER : [a-zA-Z][a-zA-Z0-9_]*;

COMMENT : '/*' .*? '*/' -> skip;

LINE_COMMENT : '//' ~[\r\n]* -> skip;

WS : [ \r\n\t] -> skip;