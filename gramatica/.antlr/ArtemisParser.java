// Generated from /home/stnin/Git/projeto-linguagem-artemis/gramatica/Artemis.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ArtemisParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, IMPORT=8, GLOBAL=9, 
		VARIABLE=10, FUNCTION=11, RETURN=12, IF=13, ELSE=14, WHILE=15, INT=16, 
		FLOAT=17, CHAR=18, STRING=19, BOOLEAN=20, STRUCT=21, TRUE=22, FALSE=23, 
		ADD=24, SUB=25, MULT=26, DIV=27, REST=28, EQUAL=29, DIFFERENT=30, BIGGER_THAN=31, 
		LESS_THAN=32, BIGGER_EQUAL_THAN=33, LESS_EQUAL_THAN=34, AND=35, OR=36, 
		ASSIGNER=37, TYPE_DELIMITER=38, LINE_DELIMITER=39, NUMBERS=40, FLOATING_POINT=41, 
		CHARACTER=42, LITERAL_STRING=43, STRUCT_IDENTIFIER=44, IDENTIFIER=45, 
		COMMENT=46, LINE_COMMENT=47, WS=48;
	public static final int
		RULE_artemis = 0, RULE_importRule = 1, RULE_externImport = 2, RULE_struct = 3, 
		RULE_mainFunction = 4, RULE_functions = 5, RULE_functionCall = 6, RULE_parametersDefinition = 7, 
		RULE_parametersPassing = 8, RULE_returnRule = 9, RULE_escope = 10, RULE_conditionals = 11, 
		RULE_ifRule = 12, RULE_elseIfRule = 13, RULE_elseRule = 14, RULE_whileLoop = 15, 
		RULE_expression = 16, RULE_intExpression = 17, RULE_floatExpression = 18, 
		RULE_charExpression = 19, RULE_stringExpression = 20, RULE_booleanExpression = 21, 
		RULE_intValues = 22, RULE_floatValues = 23, RULE_charValues = 24, RULE_stringValues = 25, 
		RULE_booleanValuesExpression = 26, RULE_allOperators = 27, RULE_logicRelationaOperators = 28, 
		RULE_sumOperators = 29, RULE_multiplicationOperators = 30, RULE_logicOperators = 31, 
		RULE_relationalOperators = 32, RULE_globalVariable = 33, RULE_localVariable = 34, 
		RULE_variableDeclaration = 35, RULE_variableAssigner = 36, RULE_structFieldAssigner = 37, 
		RULE_typeAssignerValue = 38, RULE_types = 39, RULE_values = 40, RULE_booleanValues = 41;
	public static final String[] ruleNames = {
		"artemis", "importRule", "externImport", "struct", "mainFunction", "functions", 
		"functionCall", "parametersDefinition", "parametersPassing", "returnRule", 
		"escope", "conditionals", "ifRule", "elseIfRule", "elseRule", "whileLoop", 
		"expression", "intExpression", "floatExpression", "charExpression", "stringExpression", 
		"booleanExpression", "intValues", "floatValues", "charValues", "stringValues", 
		"booleanValuesExpression", "allOperators", "logicRelationaOperators", 
		"sumOperators", "multiplicationOperators", "logicOperators", "relationalOperators", 
		"globalVariable", "localVariable", "variableDeclaration", "variableAssigner", 
		"structFieldAssigner", "typeAssignerValue", "types", "values", "booleanValues"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'{'", "'}'", "'()'", "'('", "')'", "','", "'.'", "'import'", "'global'", 
		"'variable'", "'function'", "'return'", "'if'", "'else'", "'while'", "'int'", 
		"'float'", "'char'", "'string'", "'boolean'", "'struct'", "'true'", "'false'", 
		"'+'", "'-'", "'*'", "'/'", "'%'", "'='", "'!='", "'>'", "'<'", "'>='", 
		"'<='", "'&&'", "'||'", "'<-'", "':'", "';'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, "IMPORT", "GLOBAL", "VARIABLE", 
		"FUNCTION", "RETURN", "IF", "ELSE", "WHILE", "INT", "FLOAT", "CHAR", "STRING", 
		"BOOLEAN", "STRUCT", "TRUE", "FALSE", "ADD", "SUB", "MULT", "DIV", "REST", 
		"EQUAL", "DIFFERENT", "BIGGER_THAN", "LESS_THAN", "BIGGER_EQUAL_THAN", 
		"LESS_EQUAL_THAN", "AND", "OR", "ASSIGNER", "TYPE_DELIMITER", "LINE_DELIMITER", 
		"NUMBERS", "FLOATING_POINT", "CHARACTER", "LITERAL_STRING", "STRUCT_IDENTIFIER", 
		"IDENTIFIER", "COMMENT", "LINE_COMMENT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Artemis.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ArtemisParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ArtemisContext extends ParserRuleContext {
		public ImportRuleContext importRule() {
			return getRuleContext(ImportRuleContext.class,0);
		}
		public MainFunctionContext mainFunction() {
			return getRuleContext(MainFunctionContext.class,0);
		}
		public TerminalNode EOF() { return getToken(ArtemisParser.EOF, 0); }
		public List<StructContext> struct() {
			return getRuleContexts(StructContext.class);
		}
		public StructContext struct(int i) {
			return getRuleContext(StructContext.class,i);
		}
		public List<GlobalVariableContext> globalVariable() {
			return getRuleContexts(GlobalVariableContext.class);
		}
		public GlobalVariableContext globalVariable(int i) {
			return getRuleContext(GlobalVariableContext.class,i);
		}
		public List<FunctionsContext> functions() {
			return getRuleContexts(FunctionsContext.class);
		}
		public FunctionsContext functions(int i) {
			return getRuleContext(FunctionsContext.class,i);
		}
		public ArtemisContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_artemis; }
	}

	public final ArtemisContext artemis() throws RecognitionException {
		ArtemisContext _localctx = new ArtemisContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_artemis);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(84);
			importRule();
			setState(88);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==STRUCT) {
				{
				{
				setState(85);
				struct();
				}
				}
				setState(90);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(94);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==GLOBAL) {
				{
				{
				setState(91);
				globalVariable();
				}
				}
				setState(96);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(100);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(97);
					functions();
					}
					} 
				}
				setState(102);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			setState(103);
			mainFunction();
			setState(104);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportRuleContext extends ParserRuleContext {
		public List<ExternImportContext> externImport() {
			return getRuleContexts(ExternImportContext.class);
		}
		public ExternImportContext externImport(int i) {
			return getRuleContext(ExternImportContext.class,i);
		}
		public ImportRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importRule; }
	}

	public final ImportRuleContext importRule() throws RecognitionException {
		ImportRuleContext _localctx = new ImportRuleContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_importRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==IMPORT) {
				{
				{
				setState(106);
				externImport();
				}
				}
				setState(111);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExternImportContext extends ParserRuleContext {
		public TerminalNode IMPORT() { return getToken(ArtemisParser.IMPORT, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public ExternImportContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_externImport; }
	}

	public final ExternImportContext externImport() throws RecognitionException {
		ExternImportContext _localctx = new ExternImportContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_externImport);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(112);
			match(IMPORT);
			setState(113);
			match(IDENTIFIER);
			setState(114);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StructContext extends ParserRuleContext {
		public TerminalNode STRUCT() { return getToken(ArtemisParser.STRUCT, 0); }
		public TerminalNode STRUCT_IDENTIFIER() { return getToken(ArtemisParser.STRUCT_IDENTIFIER, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public List<VariableDeclarationContext> variableDeclaration() {
			return getRuleContexts(VariableDeclarationContext.class);
		}
		public VariableDeclarationContext variableDeclaration(int i) {
			return getRuleContext(VariableDeclarationContext.class,i);
		}
		public StructContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct; }
	}

	public final StructContext struct() throws RecognitionException {
		StructContext _localctx = new StructContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_struct);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(116);
			match(STRUCT);
			setState(117);
			match(STRUCT_IDENTIFIER);
			setState(118);
			match(T__0);
			setState(120); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(119);
				variableDeclaration();
				}
				}
				setState(122); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==VARIABLE );
			setState(124);
			match(T__1);
			setState(125);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainFunctionContext extends ParserRuleContext {
		public TerminalNode FUNCTION() { return getToken(ArtemisParser.FUNCTION, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public List<EscopeContext> escope() {
			return getRuleContexts(EscopeContext.class);
		}
		public EscopeContext escope(int i) {
			return getRuleContext(EscopeContext.class,i);
		}
		public MainFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mainFunction; }
	}

	public final MainFunctionContext mainFunction() throws RecognitionException {
		MainFunctionContext _localctx = new MainFunctionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_mainFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(127);
			match(FUNCTION);
			setState(128);
			match(IDENTIFIER);
			setState(129);
			match(T__2);
			setState(130);
			match(T__0);
			setState(134);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARIABLE) | (1L << IF) | (1L << WHILE) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(131);
				escope();
				}
				}
				setState(136);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(137);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionsContext extends ParserRuleContext {
		public TerminalNode FUNCTION() { return getToken(ArtemisParser.FUNCTION, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public ParametersDefinitionContext parametersDefinition() {
			return getRuleContext(ParametersDefinitionContext.class,0);
		}
		public TerminalNode TYPE_DELIMITER() { return getToken(ArtemisParser.TYPE_DELIMITER, 0); }
		public TypesContext types() {
			return getRuleContext(TypesContext.class,0);
		}
		public List<EscopeContext> escope() {
			return getRuleContexts(EscopeContext.class);
		}
		public EscopeContext escope(int i) {
			return getRuleContext(EscopeContext.class,i);
		}
		public ReturnRuleContext returnRule() {
			return getRuleContext(ReturnRuleContext.class,0);
		}
		public FunctionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functions; }
	}

	public final FunctionsContext functions() throws RecognitionException {
		FunctionsContext _localctx = new FunctionsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_functions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(139);
			match(FUNCTION);
			setState(140);
			match(IDENTIFIER);
			setState(141);
			match(T__3);
			setState(142);
			parametersDefinition();
			setState(143);
			match(T__4);
			setState(146);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TYPE_DELIMITER) {
				{
				setState(144);
				match(TYPE_DELIMITER);
				setState(145);
				types();
				}
			}

			setState(148);
			match(T__0);
			setState(152);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARIABLE) | (1L << IF) | (1L << WHILE) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(149);
				escope();
				}
				}
				setState(154);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(156);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURN) {
				{
				setState(155);
				returnRule();
				}
			}

			setState(158);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public ParametersPassingContext parametersPassing() {
			return getRuleContext(ParametersPassingContext.class,0);
		}
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_functionCall);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			match(IDENTIFIER);
			setState(166);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__3:
				{
				setState(161);
				match(T__3);
				setState(162);
				parametersPassing();
				setState(163);
				match(T__4);
				}
				break;
			case T__2:
				{
				setState(165);
				match(T__2);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(169);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				{
				setState(168);
				match(LINE_DELIMITER);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersDefinitionContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(ArtemisParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ArtemisParser.IDENTIFIER, i);
		}
		public List<TerminalNode> TYPE_DELIMITER() { return getTokens(ArtemisParser.TYPE_DELIMITER); }
		public TerminalNode TYPE_DELIMITER(int i) {
			return getToken(ArtemisParser.TYPE_DELIMITER, i);
		}
		public List<TypesContext> types() {
			return getRuleContexts(TypesContext.class);
		}
		public TypesContext types(int i) {
			return getRuleContext(TypesContext.class,i);
		}
		public ParametersDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parametersDefinition; }
	}

	public final ParametersDefinitionContext parametersDefinition() throws RecognitionException {
		ParametersDefinitionContext _localctx = new ParametersDefinitionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_parametersDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(171);
			match(IDENTIFIER);
			setState(172);
			match(TYPE_DELIMITER);
			setState(173);
			types();
			}
			setState(181);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5) {
				{
				{
				setState(175);
				match(T__5);
				setState(176);
				match(IDENTIFIER);
				setState(177);
				match(TYPE_DELIMITER);
				setState(178);
				types();
				}
				}
				setState(183);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersPassingContext extends ParserRuleContext {
		public List<ValuesContext> values() {
			return getRuleContexts(ValuesContext.class);
		}
		public ValuesContext values(int i) {
			return getRuleContext(ValuesContext.class,i);
		}
		public ParametersPassingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parametersPassing; }
	}

	public final ParametersPassingContext parametersPassing() throws RecognitionException {
		ParametersPassingContext _localctx = new ParametersPassingContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_parametersPassing);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(184);
			values();
			setState(189);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5) {
				{
				{
				setState(185);
				match(T__5);
				setState(186);
				values();
				}
				}
				setState(191);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnRuleContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(ArtemisParser.RETURN, 0); }
		public ValuesContext values() {
			return getRuleContext(ValuesContext.class,0);
		}
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public ReturnRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnRule; }
	}

	public final ReturnRuleContext returnRule() throws RecognitionException {
		ReturnRuleContext _localctx = new ReturnRuleContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_returnRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(RETURN);
			setState(193);
			values();
			setState(194);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EscopeContext extends ParserRuleContext {
		public LocalVariableContext localVariable() {
			return getRuleContext(LocalVariableContext.class,0);
		}
		public VariableAssignerContext variableAssigner() {
			return getRuleContext(VariableAssignerContext.class,0);
		}
		public StructFieldAssignerContext structFieldAssigner() {
			return getRuleContext(StructFieldAssignerContext.class,0);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ConditionalsContext conditionals() {
			return getRuleContext(ConditionalsContext.class,0);
		}
		public WhileLoopContext whileLoop() {
			return getRuleContext(WhileLoopContext.class,0);
		}
		public EscopeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_escope; }
	}

	public final EscopeContext escope() throws RecognitionException {
		EscopeContext _localctx = new EscopeContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_escope);
		try {
			setState(202);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(196);
				localVariable();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(197);
				variableAssigner();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(198);
				structFieldAssigner();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(199);
				functionCall();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(200);
				conditionals();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(201);
				whileLoop();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionalsContext extends ParserRuleContext {
		public IfRuleContext ifRule() {
			return getRuleContext(IfRuleContext.class,0);
		}
		public ElseRuleContext elseRule() {
			return getRuleContext(ElseRuleContext.class,0);
		}
		public ConditionalsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionals; }
	}

	public final ConditionalsContext conditionals() throws RecognitionException {
		ConditionalsContext _localctx = new ConditionalsContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_conditionals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(204);
			ifRule();
			setState(206);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(205);
				elseRule();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfRuleContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(ArtemisParser.IF, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<EscopeContext> escope() {
			return getRuleContexts(EscopeContext.class);
		}
		public EscopeContext escope(int i) {
			return getRuleContext(EscopeContext.class,i);
		}
		public ReturnRuleContext returnRule() {
			return getRuleContext(ReturnRuleContext.class,0);
		}
		public ElseIfRuleContext elseIfRule() {
			return getRuleContext(ElseIfRuleContext.class,0);
		}
		public IfRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifRule; }
	}

	public final IfRuleContext ifRule() throws RecognitionException {
		IfRuleContext _localctx = new IfRuleContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_ifRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(208);
			match(IF);
			setState(209);
			match(T__3);
			setState(210);
			expression();
			setState(211);
			match(T__4);
			setState(212);
			match(T__0);
			setState(216);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARIABLE) | (1L << IF) | (1L << WHILE) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(213);
				escope();
				}
				}
				setState(218);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(220);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURN) {
				{
				setState(219);
				returnRule();
				}
			}

			setState(222);
			match(T__1);
			setState(224);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				{
				setState(223);
				elseIfRule();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseIfRuleContext extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(ArtemisParser.ELSE, 0); }
		public IfRuleContext ifRule() {
			return getRuleContext(IfRuleContext.class,0);
		}
		public ElseIfRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseIfRule; }
	}

	public final ElseIfRuleContext elseIfRule() throws RecognitionException {
		ElseIfRuleContext _localctx = new ElseIfRuleContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_elseIfRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(226);
			match(ELSE);
			setState(227);
			ifRule();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseRuleContext extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(ArtemisParser.ELSE, 0); }
		public List<EscopeContext> escope() {
			return getRuleContexts(EscopeContext.class);
		}
		public EscopeContext escope(int i) {
			return getRuleContext(EscopeContext.class,i);
		}
		public ReturnRuleContext returnRule() {
			return getRuleContext(ReturnRuleContext.class,0);
		}
		public ElseRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseRule; }
	}

	public final ElseRuleContext elseRule() throws RecognitionException {
		ElseRuleContext _localctx = new ElseRuleContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_elseRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(229);
			match(ELSE);
			setState(230);
			match(T__0);
			setState(234);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARIABLE) | (1L << IF) | (1L << WHILE) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(231);
				escope();
				}
				}
				setState(236);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(238);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURN) {
				{
				setState(237);
				returnRule();
				}
			}

			setState(240);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileLoopContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(ArtemisParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<EscopeContext> escope() {
			return getRuleContexts(EscopeContext.class);
		}
		public EscopeContext escope(int i) {
			return getRuleContext(EscopeContext.class,i);
		}
		public WhileLoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileLoop; }
	}

	public final WhileLoopContext whileLoop() throws RecognitionException {
		WhileLoopContext _localctx = new WhileLoopContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_whileLoop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(242);
			match(WHILE);
			setState(243);
			match(T__3);
			setState(244);
			expression();
			setState(245);
			match(T__4);
			setState(246);
			match(T__0);
			setState(250);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARIABLE) | (1L << IF) | (1L << WHILE) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(247);
				escope();
				}
				}
				setState(252);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(253);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public IntExpressionContext intExpression() {
			return getRuleContext(IntExpressionContext.class,0);
		}
		public FloatExpressionContext floatExpression() {
			return getRuleContext(FloatExpressionContext.class,0);
		}
		public CharExpressionContext charExpression() {
			return getRuleContext(CharExpressionContext.class,0);
		}
		public StringExpressionContext stringExpression() {
			return getRuleContext(StringExpressionContext.class,0);
		}
		public BooleanExpressionContext booleanExpression() {
			return getRuleContext(BooleanExpressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_expression);
		try {
			setState(260);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(255);
				intExpression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(256);
				floatExpression();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(257);
				charExpression();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(258);
				stringExpression();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(259);
				booleanExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntExpressionContext extends ParserRuleContext {
		public IntValuesContext intValues() {
			return getRuleContext(IntValuesContext.class,0);
		}
		public MultiplicationOperatorsContext multiplicationOperators() {
			return getRuleContext(MultiplicationOperatorsContext.class,0);
		}
		public List<IntExpressionContext> intExpression() {
			return getRuleContexts(IntExpressionContext.class);
		}
		public IntExpressionContext intExpression(int i) {
			return getRuleContext(IntExpressionContext.class,i);
		}
		public List<SumOperatorsContext> sumOperators() {
			return getRuleContexts(SumOperatorsContext.class);
		}
		public SumOperatorsContext sumOperators(int i) {
			return getRuleContext(SumOperatorsContext.class,i);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public AllOperatorsContext allOperators() {
			return getRuleContext(AllOperatorsContext.class,0);
		}
		public IntExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intExpression; }
	}

	public final IntExpressionContext intExpression() throws RecognitionException {
		IntExpressionContext _localctx = new IntExpressionContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_intExpression);
		int _la;
		try {
			int _alt;
			setState(297);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(262);
				intValues();
				setState(263);
				multiplicationOperators();
				setState(267);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(264);
						sumOperators();
						}
						} 
					}
					setState(269);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
				}
				setState(270);
				intExpression();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(272);
				intValues();
				setState(274); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(273);
						sumOperators();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(276); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(278);
				intExpression();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(280);
				intValues();
				setState(281);
				logicOperators();
				setState(282);
				intExpression();
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(284);
				intValues();
				setState(285);
				relationalOperators();
				setState(286);
				intExpression();
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(288);
				match(T__3);
				setState(289);
				intExpression();
				setState(290);
				match(T__4);
				setState(294);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ADD) | (1L << SUB) | (1L << MULT) | (1L << DIV) | (1L << REST) | (1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN) | (1L << AND) | (1L << OR))) != 0)) {
					{
					setState(291);
					allOperators();
					setState(292);
					intExpression();
					}
				}

				}
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				{
				setState(296);
				intValues();
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FloatExpressionContext extends ParserRuleContext {
		public FloatValuesContext floatValues() {
			return getRuleContext(FloatValuesContext.class,0);
		}
		public MultiplicationOperatorsContext multiplicationOperators() {
			return getRuleContext(MultiplicationOperatorsContext.class,0);
		}
		public List<FloatExpressionContext> floatExpression() {
			return getRuleContexts(FloatExpressionContext.class);
		}
		public FloatExpressionContext floatExpression(int i) {
			return getRuleContext(FloatExpressionContext.class,i);
		}
		public List<SumOperatorsContext> sumOperators() {
			return getRuleContexts(SumOperatorsContext.class);
		}
		public SumOperatorsContext sumOperators(int i) {
			return getRuleContext(SumOperatorsContext.class,i);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public AllOperatorsContext allOperators() {
			return getRuleContext(AllOperatorsContext.class,0);
		}
		public FloatExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_floatExpression; }
	}

	public final FloatExpressionContext floatExpression() throws RecognitionException {
		FloatExpressionContext _localctx = new FloatExpressionContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_floatExpression);
		int _la;
		try {
			int _alt;
			setState(334);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(299);
				floatValues();
				setState(300);
				multiplicationOperators();
				setState(304);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(301);
						sumOperators();
						}
						} 
					}
					setState(306);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
				}
				setState(307);
				floatExpression();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(309);
				floatValues();
				setState(311); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(310);
						sumOperators();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(313); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(315);
				floatExpression();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(317);
				floatValues();
				setState(318);
				logicOperators();
				setState(319);
				floatExpression();
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(321);
				floatValues();
				setState(322);
				relationalOperators();
				setState(323);
				floatExpression();
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(325);
				match(T__3);
				setState(326);
				floatExpression();
				setState(327);
				match(T__4);
				setState(331);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ADD) | (1L << SUB) | (1L << MULT) | (1L << DIV) | (1L << REST) | (1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN) | (1L << AND) | (1L << OR))) != 0)) {
					{
					setState(328);
					allOperators();
					setState(329);
					floatExpression();
					}
				}

				}
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				{
				setState(333);
				floatValues();
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharExpressionContext extends ParserRuleContext {
		public CharValuesContext charValues() {
			return getRuleContext(CharValuesContext.class,0);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public List<CharExpressionContext> charExpression() {
			return getRuleContexts(CharExpressionContext.class);
		}
		public CharExpressionContext charExpression(int i) {
			return getRuleContext(CharExpressionContext.class,i);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public LogicRelationaOperatorsContext logicRelationaOperators() {
			return getRuleContext(LogicRelationaOperatorsContext.class,0);
		}
		public CharExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_charExpression; }
	}

	public final CharExpressionContext charExpression() throws RecognitionException {
		CharExpressionContext _localctx = new CharExpressionContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_charExpression);
		int _la;
		try {
			setState(353);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(336);
				charValues();
				setState(337);
				logicOperators();
				setState(338);
				charExpression();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(340);
				charValues();
				setState(341);
				relationalOperators();
				setState(342);
				charExpression();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(344);
				match(T__3);
				setState(345);
				charExpression();
				setState(346);
				match(T__4);
				setState(350);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN) | (1L << AND) | (1L << OR))) != 0)) {
					{
					setState(347);
					logicRelationaOperators();
					setState(348);
					charExpression();
					}
				}

				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(352);
				charValues();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringExpressionContext extends ParserRuleContext {
		public StringValuesContext stringValues() {
			return getRuleContext(StringValuesContext.class,0);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public List<StringExpressionContext> stringExpression() {
			return getRuleContexts(StringExpressionContext.class);
		}
		public StringExpressionContext stringExpression(int i) {
			return getRuleContext(StringExpressionContext.class,i);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public LogicRelationaOperatorsContext logicRelationaOperators() {
			return getRuleContext(LogicRelationaOperatorsContext.class,0);
		}
		public StringExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringExpression; }
	}

	public final StringExpressionContext stringExpression() throws RecognitionException {
		StringExpressionContext _localctx = new StringExpressionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_stringExpression);
		int _la;
		try {
			setState(372);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(355);
				stringValues();
				setState(356);
				logicOperators();
				setState(357);
				stringExpression();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(359);
				stringValues();
				setState(360);
				relationalOperators();
				setState(361);
				stringExpression();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(363);
				match(T__3);
				setState(364);
				stringExpression();
				setState(365);
				match(T__4);
				setState(369);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN) | (1L << AND) | (1L << OR))) != 0)) {
					{
					setState(366);
					logicRelationaOperators();
					setState(367);
					stringExpression();
					}
				}

				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(371);
				stringValues();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanExpressionContext extends ParserRuleContext {
		public BooleanValuesExpressionContext booleanValuesExpression() {
			return getRuleContext(BooleanValuesExpressionContext.class,0);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public List<BooleanExpressionContext> booleanExpression() {
			return getRuleContexts(BooleanExpressionContext.class);
		}
		public BooleanExpressionContext booleanExpression(int i) {
			return getRuleContext(BooleanExpressionContext.class,i);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public LogicRelationaOperatorsContext logicRelationaOperators() {
			return getRuleContext(LogicRelationaOperatorsContext.class,0);
		}
		public BooleanExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanExpression; }
	}

	public final BooleanExpressionContext booleanExpression() throws RecognitionException {
		BooleanExpressionContext _localctx = new BooleanExpressionContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_booleanExpression);
		int _la;
		try {
			setState(391);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(374);
				booleanValuesExpression();
				setState(375);
				logicOperators();
				setState(376);
				booleanExpression();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(378);
				booleanValuesExpression();
				setState(379);
				relationalOperators();
				setState(380);
				booleanExpression();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(382);
				match(T__3);
				setState(383);
				booleanExpression();
				setState(384);
				match(T__4);
				setState(388);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN) | (1L << AND) | (1L << OR))) != 0)) {
					{
					setState(385);
					logicRelationaOperators();
					setState(386);
					booleanExpression();
					}
				}

				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(390);
				booleanValuesExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntValuesContext extends ParserRuleContext {
		public TerminalNode NUMBERS() { return getToken(ArtemisParser.NUMBERS, 0); }
		public List<SumOperatorsContext> sumOperators() {
			return getRuleContexts(SumOperatorsContext.class);
		}
		public SumOperatorsContext sumOperators(int i) {
			return getRuleContext(SumOperatorsContext.class,i);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public IntValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intValues; }
	}

	public final IntValuesContext intValues() throws RecognitionException {
		IntValuesContext _localctx = new IntValuesContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_intValues);
		int _la;
		try {
			setState(402);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,37,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(396);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==ADD || _la==SUB) {
					{
					{
					setState(393);
					sumOperators();
					}
					}
					setState(398);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(399);
				match(NUMBERS);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(400);
				functionCall();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(401);
				match(IDENTIFIER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FloatValuesContext extends ParserRuleContext {
		public TerminalNode NUMBERS() { return getToken(ArtemisParser.NUMBERS, 0); }
		public List<SumOperatorsContext> sumOperators() {
			return getRuleContexts(SumOperatorsContext.class);
		}
		public SumOperatorsContext sumOperators(int i) {
			return getRuleContext(SumOperatorsContext.class,i);
		}
		public TerminalNode FLOATING_POINT() { return getToken(ArtemisParser.FLOATING_POINT, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FloatValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_floatValues; }
	}

	public final FloatValuesContext floatValues() throws RecognitionException {
		FloatValuesContext _localctx = new FloatValuesContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_floatValues);
		int _la;
		try {
			setState(420);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(407);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==ADD || _la==SUB) {
					{
					{
					setState(404);
					sumOperators();
					}
					}
					setState(409);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(410);
				match(NUMBERS);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(414);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==ADD || _la==SUB) {
					{
					{
					setState(411);
					sumOperators();
					}
					}
					setState(416);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(417);
				match(FLOATING_POINT);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(418);
				functionCall();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(419);
				match(IDENTIFIER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharValuesContext extends ParserRuleContext {
		public TerminalNode CHARACTER() { return getToken(ArtemisParser.CHARACTER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public CharValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_charValues; }
	}

	public final CharValuesContext charValues() throws RecognitionException {
		CharValuesContext _localctx = new CharValuesContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_charValues);
		try {
			setState(425);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(422);
				match(CHARACTER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(423);
				match(IDENTIFIER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(424);
				functionCall();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringValuesContext extends ParserRuleContext {
		public TerminalNode LITERAL_STRING() { return getToken(ArtemisParser.LITERAL_STRING, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public StringValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringValues; }
	}

	public final StringValuesContext stringValues() throws RecognitionException {
		StringValuesContext _localctx = new StringValuesContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_stringValues);
		try {
			setState(430);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(427);
				match(LITERAL_STRING);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(428);
				match(IDENTIFIER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(429);
				functionCall();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanValuesExpressionContext extends ParserRuleContext {
		public BooleanValuesContext booleanValues() {
			return getRuleContext(BooleanValuesContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public BooleanValuesExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanValuesExpression; }
	}

	public final BooleanValuesExpressionContext booleanValuesExpression() throws RecognitionException {
		BooleanValuesExpressionContext _localctx = new BooleanValuesExpressionContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_booleanValuesExpression);
		try {
			setState(435);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(432);
				booleanValues();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(433);
				match(IDENTIFIER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(434);
				functionCall();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AllOperatorsContext extends ParserRuleContext {
		public SumOperatorsContext sumOperators() {
			return getRuleContext(SumOperatorsContext.class,0);
		}
		public MultiplicationOperatorsContext multiplicationOperators() {
			return getRuleContext(MultiplicationOperatorsContext.class,0);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public AllOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_allOperators; }
	}

	public final AllOperatorsContext allOperators() throws RecognitionException {
		AllOperatorsContext _localctx = new AllOperatorsContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_allOperators);
		try {
			setState(441);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ADD:
			case SUB:
				enterOuterAlt(_localctx, 1);
				{
				setState(437);
				sumOperators();
				}
				break;
			case MULT:
			case DIV:
			case REST:
				enterOuterAlt(_localctx, 2);
				{
				setState(438);
				multiplicationOperators();
				}
				break;
			case AND:
			case OR:
				enterOuterAlt(_localctx, 3);
				{
				setState(439);
				logicOperators();
				}
				break;
			case EQUAL:
			case DIFFERENT:
			case BIGGER_THAN:
			case LESS_THAN:
			case BIGGER_EQUAL_THAN:
			case LESS_EQUAL_THAN:
				enterOuterAlt(_localctx, 4);
				{
				setState(440);
				relationalOperators();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicRelationaOperatorsContext extends ParserRuleContext {
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public LogicRelationaOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicRelationaOperators; }
	}

	public final LogicRelationaOperatorsContext logicRelationaOperators() throws RecognitionException {
		LogicRelationaOperatorsContext _localctx = new LogicRelationaOperatorsContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_logicRelationaOperators);
		try {
			setState(445);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case AND:
			case OR:
				enterOuterAlt(_localctx, 1);
				{
				setState(443);
				logicOperators();
				}
				break;
			case EQUAL:
			case DIFFERENT:
			case BIGGER_THAN:
			case LESS_THAN:
			case BIGGER_EQUAL_THAN:
			case LESS_EQUAL_THAN:
				enterOuterAlt(_localctx, 2);
				{
				setState(444);
				relationalOperators();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SumOperatorsContext extends ParserRuleContext {
		public TerminalNode ADD() { return getToken(ArtemisParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(ArtemisParser.SUB, 0); }
		public SumOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sumOperators; }
	}

	public final SumOperatorsContext sumOperators() throws RecognitionException {
		SumOperatorsContext _localctx = new SumOperatorsContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_sumOperators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(447);
			_la = _input.LA(1);
			if ( !(_la==ADD || _la==SUB) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplicationOperatorsContext extends ParserRuleContext {
		public TerminalNode MULT() { return getToken(ArtemisParser.MULT, 0); }
		public TerminalNode DIV() { return getToken(ArtemisParser.DIV, 0); }
		public TerminalNode REST() { return getToken(ArtemisParser.REST, 0); }
		public MultiplicationOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicationOperators; }
	}

	public final MultiplicationOperatorsContext multiplicationOperators() throws RecognitionException {
		MultiplicationOperatorsContext _localctx = new MultiplicationOperatorsContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_multiplicationOperators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(449);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MULT) | (1L << DIV) | (1L << REST))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicOperatorsContext extends ParserRuleContext {
		public TerminalNode AND() { return getToken(ArtemisParser.AND, 0); }
		public TerminalNode OR() { return getToken(ArtemisParser.OR, 0); }
		public LogicOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicOperators; }
	}

	public final LogicOperatorsContext logicOperators() throws RecognitionException {
		LogicOperatorsContext _localctx = new LogicOperatorsContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_logicOperators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(451);
			_la = _input.LA(1);
			if ( !(_la==AND || _la==OR) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalOperatorsContext extends ParserRuleContext {
		public TerminalNode EQUAL() { return getToken(ArtemisParser.EQUAL, 0); }
		public TerminalNode DIFFERENT() { return getToken(ArtemisParser.DIFFERENT, 0); }
		public TerminalNode BIGGER_THAN() { return getToken(ArtemisParser.BIGGER_THAN, 0); }
		public TerminalNode BIGGER_EQUAL_THAN() { return getToken(ArtemisParser.BIGGER_EQUAL_THAN, 0); }
		public TerminalNode LESS_THAN() { return getToken(ArtemisParser.LESS_THAN, 0); }
		public TerminalNode LESS_EQUAL_THAN() { return getToken(ArtemisParser.LESS_EQUAL_THAN, 0); }
		public RelationalOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalOperators; }
	}

	public final RelationalOperatorsContext relationalOperators() throws RecognitionException {
		RelationalOperatorsContext _localctx = new RelationalOperatorsContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_relationalOperators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(453);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GlobalVariableContext extends ParserRuleContext {
		public TerminalNode GLOBAL() { return getToken(ArtemisParser.GLOBAL, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public TerminalNode TYPE_DELIMITER() { return getToken(ArtemisParser.TYPE_DELIMITER, 0); }
		public TypeAssignerValueContext typeAssignerValue() {
			return getRuleContext(TypeAssignerValueContext.class,0);
		}
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public GlobalVariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_globalVariable; }
	}

	public final GlobalVariableContext globalVariable() throws RecognitionException {
		GlobalVariableContext _localctx = new GlobalVariableContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_globalVariable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(455);
			match(GLOBAL);
			setState(456);
			match(IDENTIFIER);
			setState(457);
			match(TYPE_DELIMITER);
			setState(458);
			typeAssignerValue();
			setState(459);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LocalVariableContext extends ParserRuleContext {
		public TerminalNode VARIABLE() { return getToken(ArtemisParser.VARIABLE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public TerminalNode TYPE_DELIMITER() { return getToken(ArtemisParser.TYPE_DELIMITER, 0); }
		public TypeAssignerValueContext typeAssignerValue() {
			return getRuleContext(TypeAssignerValueContext.class,0);
		}
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public LocalVariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_localVariable; }
	}

	public final LocalVariableContext localVariable() throws RecognitionException {
		LocalVariableContext _localctx = new LocalVariableContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_localVariable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(461);
			match(VARIABLE);
			setState(462);
			match(IDENTIFIER);
			setState(463);
			match(TYPE_DELIMITER);
			setState(464);
			typeAssignerValue();
			setState(465);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclarationContext extends ParserRuleContext {
		public TerminalNode VARIABLE() { return getToken(ArtemisParser.VARIABLE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public TerminalNode TYPE_DELIMITER() { return getToken(ArtemisParser.TYPE_DELIMITER, 0); }
		public TypesContext types() {
			return getRuleContext(TypesContext.class,0);
		}
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public VariableDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclaration; }
	}

	public final VariableDeclarationContext variableDeclaration() throws RecognitionException {
		VariableDeclarationContext _localctx = new VariableDeclarationContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_variableDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(467);
			match(VARIABLE);
			setState(468);
			match(IDENTIFIER);
			setState(469);
			match(TYPE_DELIMITER);
			setState(470);
			types();
			setState(471);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableAssignerContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGNER() { return getToken(ArtemisParser.ASSIGNER, 0); }
		public ValuesContext values() {
			return getRuleContext(ValuesContext.class,0);
		}
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public VariableAssignerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableAssigner; }
	}

	public final VariableAssignerContext variableAssigner() throws RecognitionException {
		VariableAssignerContext _localctx = new VariableAssignerContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_variableAssigner);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(473);
			match(IDENTIFIER);
			setState(474);
			match(ASSIGNER);
			setState(475);
			values();
			setState(476);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StructFieldAssignerContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(ArtemisParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ArtemisParser.IDENTIFIER, i);
		}
		public TerminalNode ASSIGNER() { return getToken(ArtemisParser.ASSIGNER, 0); }
		public ValuesContext values() {
			return getRuleContext(ValuesContext.class,0);
		}
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public StructFieldAssignerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structFieldAssigner; }
	}

	public final StructFieldAssignerContext structFieldAssigner() throws RecognitionException {
		StructFieldAssignerContext _localctx = new StructFieldAssignerContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_structFieldAssigner);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(478);
			match(IDENTIFIER);
			setState(479);
			match(T__6);
			setState(480);
			match(IDENTIFIER);
			setState(481);
			match(ASSIGNER);
			setState(482);
			values();
			setState(483);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeAssignerValueContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(ArtemisParser.INT, 0); }
		public TerminalNode ASSIGNER() { return getToken(ArtemisParser.ASSIGNER, 0); }
		public TerminalNode NUMBERS() { return getToken(ArtemisParser.NUMBERS, 0); }
		public IntExpressionContext intExpression() {
			return getRuleContext(IntExpressionContext.class,0);
		}
		public TerminalNode FLOAT() { return getToken(ArtemisParser.FLOAT, 0); }
		public TerminalNode FLOATING_POINT() { return getToken(ArtemisParser.FLOATING_POINT, 0); }
		public FloatExpressionContext floatExpression() {
			return getRuleContext(FloatExpressionContext.class,0);
		}
		public TerminalNode CHAR() { return getToken(ArtemisParser.CHAR, 0); }
		public TerminalNode CHARACTER() { return getToken(ArtemisParser.CHARACTER, 0); }
		public TerminalNode STRING() { return getToken(ArtemisParser.STRING, 0); }
		public TerminalNode LITERAL_STRING() { return getToken(ArtemisParser.LITERAL_STRING, 0); }
		public TerminalNode BOOLEAN() { return getToken(ArtemisParser.BOOLEAN, 0); }
		public BooleanValuesContext booleanValues() {
			return getRuleContext(BooleanValuesContext.class,0);
		}
		public BooleanExpressionContext booleanExpression() {
			return getRuleContext(BooleanExpressionContext.class,0);
		}
		public TerminalNode STRUCT_IDENTIFIER() { return getToken(ArtemisParser.STRUCT_IDENTIFIER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public TypeAssignerValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeAssignerValue; }
	}

	public final TypeAssignerValueContext typeAssignerValue() throws RecognitionException {
		TypeAssignerValueContext _localctx = new TypeAssignerValueContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_typeAssignerValue);
		try {
			setState(514);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,49,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(485);
				match(INT);
				setState(486);
				match(ASSIGNER);
				setState(489);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
				case 1:
					{
					setState(487);
					match(NUMBERS);
					}
					break;
				case 2:
					{
					setState(488);
					intExpression();
					}
					break;
				}
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(491);
				match(FLOAT);
				setState(492);
				match(ASSIGNER);
				setState(495);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,47,_ctx) ) {
				case 1:
					{
					setState(493);
					match(FLOATING_POINT);
					}
					break;
				case 2:
					{
					setState(494);
					floatExpression();
					}
					break;
				}
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(497);
				match(CHAR);
				setState(498);
				match(ASSIGNER);
				setState(499);
				match(CHARACTER);
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(500);
				match(STRING);
				setState(501);
				match(ASSIGNER);
				setState(502);
				match(LITERAL_STRING);
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(503);
				match(BOOLEAN);
				setState(504);
				match(ASSIGNER);
				setState(507);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,48,_ctx) ) {
				case 1:
					{
					setState(505);
					booleanValues();
					}
					break;
				case 2:
					{
					setState(506);
					booleanExpression();
					}
					break;
				}
				}
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				{
				setState(509);
				match(STRUCT_IDENTIFIER);
				setState(510);
				match(ASSIGNER);
				setState(511);
				match(IDENTIFIER);
				}
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(512);
				match(IDENTIFIER);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(513);
				functionCall();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypesContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(ArtemisParser.INT, 0); }
		public TerminalNode FLOAT() { return getToken(ArtemisParser.FLOAT, 0); }
		public TerminalNode CHAR() { return getToken(ArtemisParser.CHAR, 0); }
		public TerminalNode STRING() { return getToken(ArtemisParser.STRING, 0); }
		public TerminalNode BOOLEAN() { return getToken(ArtemisParser.BOOLEAN, 0); }
		public TerminalNode STRUCT_IDENTIFIER() { return getToken(ArtemisParser.STRUCT_IDENTIFIER, 0); }
		public TypesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_types; }
	}

	public final TypesContext types() throws RecognitionException {
		TypesContext _localctx = new TypesContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_types);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(516);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT) | (1L << FLOAT) | (1L << CHAR) | (1L << STRING) | (1L << BOOLEAN) | (1L << STRUCT_IDENTIFIER))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValuesContext extends ParserRuleContext {
		public TerminalNode NUMBERS() { return getToken(ArtemisParser.NUMBERS, 0); }
		public TerminalNode FLOATING_POINT() { return getToken(ArtemisParser.FLOATING_POINT, 0); }
		public TerminalNode CHARACTER() { return getToken(ArtemisParser.CHARACTER, 0); }
		public TerminalNode LITERAL_STRING() { return getToken(ArtemisParser.LITERAL_STRING, 0); }
		public TerminalNode TRUE() { return getToken(ArtemisParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(ArtemisParser.FALSE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_values; }
	}

	public final ValuesContext values() throws RecognitionException {
		ValuesContext _localctx = new ValuesContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_values);
		try {
			setState(527);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,50,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(518);
				match(NUMBERS);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(519);
				match(FLOATING_POINT);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(520);
				match(CHARACTER);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(521);
				match(LITERAL_STRING);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(522);
				match(TRUE);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(523);
				match(FALSE);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(524);
				match(IDENTIFIER);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(525);
				functionCall();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(526);
				expression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanValuesContext extends ParserRuleContext {
		public TerminalNode TRUE() { return getToken(ArtemisParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(ArtemisParser.FALSE, 0); }
		public BooleanValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanValues; }
	}

	public final BooleanValuesContext booleanValues() throws RecognitionException {
		BooleanValuesContext _localctx = new BooleanValuesContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_booleanValues);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(529);
			_la = _input.LA(1);
			if ( !(_la==TRUE || _la==FALSE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\62\u0216\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\3"+
		"\2\3\2\7\2Y\n\2\f\2\16\2\\\13\2\3\2\7\2_\n\2\f\2\16\2b\13\2\3\2\7\2e\n"+
		"\2\f\2\16\2h\13\2\3\2\3\2\3\2\3\3\7\3n\n\3\f\3\16\3q\13\3\3\4\3\4\3\4"+
		"\3\4\3\5\3\5\3\5\3\5\6\5{\n\5\r\5\16\5|\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3"+
		"\6\7\6\u0087\n\6\f\6\16\6\u008a\13\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\5\7\u0095\n\7\3\7\3\7\7\7\u0099\n\7\f\7\16\7\u009c\13\7\3\7\5\7\u009f"+
		"\n\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00a9\n\b\3\b\5\b\u00ac\n\b\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u00b6\n\t\f\t\16\t\u00b9\13\t\3\n\3"+
		"\n\3\n\7\n\u00be\n\n\f\n\16\n\u00c1\13\n\3\13\3\13\3\13\3\13\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\5\f\u00cd\n\f\3\r\3\r\5\r\u00d1\n\r\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\7\16\u00d9\n\16\f\16\16\16\u00dc\13\16\3\16\5\16\u00df\n\16"+
		"\3\16\3\16\5\16\u00e3\n\16\3\17\3\17\3\17\3\20\3\20\3\20\7\20\u00eb\n"+
		"\20\f\20\16\20\u00ee\13\20\3\20\5\20\u00f1\n\20\3\20\3\20\3\21\3\21\3"+
		"\21\3\21\3\21\3\21\7\21\u00fb\n\21\f\21\16\21\u00fe\13\21\3\21\3\21\3"+
		"\22\3\22\3\22\3\22\3\22\5\22\u0107\n\22\3\23\3\23\3\23\7\23\u010c\n\23"+
		"\f\23\16\23\u010f\13\23\3\23\3\23\3\23\3\23\6\23\u0115\n\23\r\23\16\23"+
		"\u0116\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\5\23\u0129\n\23\3\23\5\23\u012c\n\23\3\24\3\24\3\24\7"+
		"\24\u0131\n\24\f\24\16\24\u0134\13\24\3\24\3\24\3\24\3\24\6\24\u013a\n"+
		"\24\r\24\16\24\u013b\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u014e\n\24\3\24\5\24\u0151\n\24\3"+
		"\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\5"+
		"\25\u0161\n\25\3\25\5\25\u0164\n\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u0174\n\26\3\26\5\26\u0177\n"+
		"\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3"+
		"\27\5\27\u0187\n\27\3\27\5\27\u018a\n\27\3\30\7\30\u018d\n\30\f\30\16"+
		"\30\u0190\13\30\3\30\3\30\3\30\5\30\u0195\n\30\3\31\7\31\u0198\n\31\f"+
		"\31\16\31\u019b\13\31\3\31\3\31\7\31\u019f\n\31\f\31\16\31\u01a2\13\31"+
		"\3\31\3\31\3\31\5\31\u01a7\n\31\3\32\3\32\3\32\5\32\u01ac\n\32\3\33\3"+
		"\33\3\33\5\33\u01b1\n\33\3\34\3\34\3\34\5\34\u01b6\n\34\3\35\3\35\3\35"+
		"\3\35\5\35\u01bc\n\35\3\36\3\36\5\36\u01c0\n\36\3\37\3\37\3 \3 \3!\3!"+
		"\3\"\3\"\3#\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3%\3%\3%\3%\3%\3%\3&\3&\3"+
		"&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3(\3(\3(\3(\5(\u01ec\n(\3(\3(\3(\3"+
		"(\5(\u01f2\n(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\5(\u01fe\n(\3(\3(\3(\3(\3"+
		"(\5(\u0205\n(\3)\3)\3*\3*\3*\3*\3*\3*\3*\3*\3*\5*\u0212\n*\3+\3+\3+\2"+
		"\2,\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@B"+
		"DFHJLNPRT\2\b\3\2\32\33\3\2\34\36\3\2%&\3\2\37$\4\2\22\26..\3\2\30\31"+
		"\2\u0248\2V\3\2\2\2\4o\3\2\2\2\6r\3\2\2\2\bv\3\2\2\2\n\u0081\3\2\2\2\f"+
		"\u008d\3\2\2\2\16\u00a2\3\2\2\2\20\u00ad\3\2\2\2\22\u00ba\3\2\2\2\24\u00c2"+
		"\3\2\2\2\26\u00cc\3\2\2\2\30\u00ce\3\2\2\2\32\u00d2\3\2\2\2\34\u00e4\3"+
		"\2\2\2\36\u00e7\3\2\2\2 \u00f4\3\2\2\2\"\u0106\3\2\2\2$\u012b\3\2\2\2"+
		"&\u0150\3\2\2\2(\u0163\3\2\2\2*\u0176\3\2\2\2,\u0189\3\2\2\2.\u0194\3"+
		"\2\2\2\60\u01a6\3\2\2\2\62\u01ab\3\2\2\2\64\u01b0\3\2\2\2\66\u01b5\3\2"+
		"\2\28\u01bb\3\2\2\2:\u01bf\3\2\2\2<\u01c1\3\2\2\2>\u01c3\3\2\2\2@\u01c5"+
		"\3\2\2\2B\u01c7\3\2\2\2D\u01c9\3\2\2\2F\u01cf\3\2\2\2H\u01d5\3\2\2\2J"+
		"\u01db\3\2\2\2L\u01e0\3\2\2\2N\u0204\3\2\2\2P\u0206\3\2\2\2R\u0211\3\2"+
		"\2\2T\u0213\3\2\2\2VZ\5\4\3\2WY\5\b\5\2XW\3\2\2\2Y\\\3\2\2\2ZX\3\2\2\2"+
		"Z[\3\2\2\2[`\3\2\2\2\\Z\3\2\2\2]_\5D#\2^]\3\2\2\2_b\3\2\2\2`^\3\2\2\2"+
		"`a\3\2\2\2af\3\2\2\2b`\3\2\2\2ce\5\f\7\2dc\3\2\2\2eh\3\2\2\2fd\3\2\2\2"+
		"fg\3\2\2\2gi\3\2\2\2hf\3\2\2\2ij\5\n\6\2jk\7\2\2\3k\3\3\2\2\2ln\5\6\4"+
		"\2ml\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2p\5\3\2\2\2qo\3\2\2\2rs\7\n"+
		"\2\2st\7/\2\2tu\7)\2\2u\7\3\2\2\2vw\7\27\2\2wx\7.\2\2xz\7\3\2\2y{\5H%"+
		"\2zy\3\2\2\2{|\3\2\2\2|z\3\2\2\2|}\3\2\2\2}~\3\2\2\2~\177\7\4\2\2\177"+
		"\u0080\7)\2\2\u0080\t\3\2\2\2\u0081\u0082\7\r\2\2\u0082\u0083\7/\2\2\u0083"+
		"\u0084\7\5\2\2\u0084\u0088\7\3\2\2\u0085\u0087\5\26\f\2\u0086\u0085\3"+
		"\2\2\2\u0087\u008a\3\2\2\2\u0088\u0086\3\2\2\2\u0088\u0089\3\2\2\2\u0089"+
		"\u008b\3\2\2\2\u008a\u0088\3\2\2\2\u008b\u008c\7\4\2\2\u008c\13\3\2\2"+
		"\2\u008d\u008e\7\r\2\2\u008e\u008f\7/\2\2\u008f\u0090\7\6\2\2\u0090\u0091"+
		"\5\20\t\2\u0091\u0094\7\7\2\2\u0092\u0093\7(\2\2\u0093\u0095\5P)\2\u0094"+
		"\u0092\3\2\2\2\u0094\u0095\3\2\2\2\u0095\u0096\3\2\2\2\u0096\u009a\7\3"+
		"\2\2\u0097\u0099\5\26\f\2\u0098\u0097\3\2\2\2\u0099\u009c\3\2\2\2\u009a"+
		"\u0098\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009e\3\2\2\2\u009c\u009a\3\2"+
		"\2\2\u009d\u009f\5\24\13\2\u009e\u009d\3\2\2\2\u009e\u009f\3\2\2\2\u009f"+
		"\u00a0\3\2\2\2\u00a0\u00a1\7\4\2\2\u00a1\r\3\2\2\2\u00a2\u00a8\7/\2\2"+
		"\u00a3\u00a4\7\6\2\2\u00a4\u00a5\5\22\n\2\u00a5\u00a6\7\7\2\2\u00a6\u00a9"+
		"\3\2\2\2\u00a7\u00a9\7\5\2\2\u00a8\u00a3\3\2\2\2\u00a8\u00a7\3\2\2\2\u00a9"+
		"\u00ab\3\2\2\2\u00aa\u00ac\7)\2\2\u00ab\u00aa\3\2\2\2\u00ab\u00ac\3\2"+
		"\2\2\u00ac\17\3\2\2\2\u00ad\u00ae\7/\2\2\u00ae\u00af\7(\2\2\u00af\u00b0"+
		"\5P)\2\u00b0\u00b7\3\2\2\2\u00b1\u00b2\7\b\2\2\u00b2\u00b3\7/\2\2\u00b3"+
		"\u00b4\7(\2\2\u00b4\u00b6\5P)\2\u00b5\u00b1\3\2\2\2\u00b6\u00b9\3\2\2"+
		"\2\u00b7\u00b5\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8\21\3\2\2\2\u00b9\u00b7"+
		"\3\2\2\2\u00ba\u00bf\5R*\2\u00bb\u00bc\7\b\2\2\u00bc\u00be\5R*\2\u00bd"+
		"\u00bb\3\2\2\2\u00be\u00c1\3\2\2\2\u00bf\u00bd\3\2\2\2\u00bf\u00c0\3\2"+
		"\2\2\u00c0\23\3\2\2\2\u00c1\u00bf\3\2\2\2\u00c2\u00c3\7\16\2\2\u00c3\u00c4"+
		"\5R*\2\u00c4\u00c5\7)\2\2\u00c5\25\3\2\2\2\u00c6\u00cd\5F$\2\u00c7\u00cd"+
		"\5J&\2\u00c8\u00cd\5L\'\2\u00c9\u00cd\5\16\b\2\u00ca\u00cd\5\30\r\2\u00cb"+
		"\u00cd\5 \21\2\u00cc\u00c6\3\2\2\2\u00cc\u00c7\3\2\2\2\u00cc\u00c8\3\2"+
		"\2\2\u00cc\u00c9\3\2\2\2\u00cc\u00ca\3\2\2\2\u00cc\u00cb\3\2\2\2\u00cd"+
		"\27\3\2\2\2\u00ce\u00d0\5\32\16\2\u00cf\u00d1\5\36\20\2\u00d0\u00cf\3"+
		"\2\2\2\u00d0\u00d1\3\2\2\2\u00d1\31\3\2\2\2\u00d2\u00d3\7\17\2\2\u00d3"+
		"\u00d4\7\6\2\2\u00d4\u00d5\5\"\22\2\u00d5\u00d6\7\7\2\2\u00d6\u00da\7"+
		"\3\2\2\u00d7\u00d9\5\26\f\2\u00d8\u00d7\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da"+
		"\u00d8\3\2\2\2\u00da\u00db\3\2\2\2\u00db\u00de\3\2\2\2\u00dc\u00da\3\2"+
		"\2\2\u00dd\u00df\5\24\13\2\u00de\u00dd\3\2\2\2\u00de\u00df\3\2\2\2\u00df"+
		"\u00e0\3\2\2\2\u00e0\u00e2\7\4\2\2\u00e1\u00e3\5\34\17\2\u00e2\u00e1\3"+
		"\2\2\2\u00e2\u00e3\3\2\2\2\u00e3\33\3\2\2\2\u00e4\u00e5\7\20\2\2\u00e5"+
		"\u00e6\5\32\16\2\u00e6\35\3\2\2\2\u00e7\u00e8\7\20\2\2\u00e8\u00ec\7\3"+
		"\2\2\u00e9\u00eb\5\26\f\2\u00ea\u00e9\3\2\2\2\u00eb\u00ee\3\2\2\2\u00ec"+
		"\u00ea\3\2\2\2\u00ec\u00ed\3\2\2\2\u00ed\u00f0\3\2\2\2\u00ee\u00ec\3\2"+
		"\2\2\u00ef\u00f1\5\24\13\2\u00f0\u00ef\3\2\2\2\u00f0\u00f1\3\2\2\2\u00f1"+
		"\u00f2\3\2\2\2\u00f2\u00f3\7\4\2\2\u00f3\37\3\2\2\2\u00f4\u00f5\7\21\2"+
		"\2\u00f5\u00f6\7\6\2\2\u00f6\u00f7\5\"\22\2\u00f7\u00f8\7\7\2\2\u00f8"+
		"\u00fc\7\3\2\2\u00f9\u00fb\5\26\f\2\u00fa\u00f9\3\2\2\2\u00fb\u00fe\3"+
		"\2\2\2\u00fc\u00fa\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\u00ff\3\2\2\2\u00fe"+
		"\u00fc\3\2\2\2\u00ff\u0100\7\4\2\2\u0100!\3\2\2\2\u0101\u0107\5$\23\2"+
		"\u0102\u0107\5&\24\2\u0103\u0107\5(\25\2\u0104\u0107\5*\26\2\u0105\u0107"+
		"\5,\27\2\u0106\u0101\3\2\2\2\u0106\u0102\3\2\2\2\u0106\u0103\3\2\2\2\u0106"+
		"\u0104\3\2\2\2\u0106\u0105\3\2\2\2\u0107#\3\2\2\2\u0108\u0109\5.\30\2"+
		"\u0109\u010d\5> \2\u010a\u010c\5<\37\2\u010b\u010a\3\2\2\2\u010c\u010f"+
		"\3\2\2\2\u010d\u010b\3\2\2\2\u010d\u010e\3\2\2\2\u010e\u0110\3\2\2\2\u010f"+
		"\u010d\3\2\2\2\u0110\u0111\5$\23\2\u0111\u012c\3\2\2\2\u0112\u0114\5."+
		"\30\2\u0113\u0115\5<\37\2\u0114\u0113\3\2\2\2\u0115\u0116\3\2\2\2\u0116"+
		"\u0114\3\2\2\2\u0116\u0117\3\2\2\2\u0117\u0118\3\2\2\2\u0118\u0119\5$"+
		"\23\2\u0119\u012c\3\2\2\2\u011a\u011b\5.\30\2\u011b\u011c\5@!\2\u011c"+
		"\u011d\5$\23\2\u011d\u012c\3\2\2\2\u011e\u011f\5.\30\2\u011f\u0120\5B"+
		"\"\2\u0120\u0121\5$\23\2\u0121\u012c\3\2\2\2\u0122\u0123\7\6\2\2\u0123"+
		"\u0124\5$\23\2\u0124\u0128\7\7\2\2\u0125\u0126\58\35\2\u0126\u0127\5$"+
		"\23\2\u0127\u0129\3\2\2\2\u0128\u0125\3\2\2\2\u0128\u0129\3\2\2\2\u0129"+
		"\u012c\3\2\2\2\u012a\u012c\5.\30\2\u012b\u0108\3\2\2\2\u012b\u0112\3\2"+
		"\2\2\u012b\u011a\3\2\2\2\u012b\u011e\3\2\2\2\u012b\u0122\3\2\2\2\u012b"+
		"\u012a\3\2\2\2\u012c%\3\2\2\2\u012d\u012e\5\60\31\2\u012e\u0132\5> \2"+
		"\u012f\u0131\5<\37\2\u0130\u012f\3\2\2\2\u0131\u0134\3\2\2\2\u0132\u0130"+
		"\3\2\2\2\u0132\u0133\3\2\2\2\u0133\u0135\3\2\2\2\u0134\u0132\3\2\2\2\u0135"+
		"\u0136\5&\24\2\u0136\u0151\3\2\2\2\u0137\u0139\5\60\31\2\u0138\u013a\5"+
		"<\37\2\u0139\u0138\3\2\2\2\u013a\u013b\3\2\2\2\u013b\u0139\3\2\2\2\u013b"+
		"\u013c\3\2\2\2\u013c\u013d\3\2\2\2\u013d\u013e\5&\24\2\u013e\u0151\3\2"+
		"\2\2\u013f\u0140\5\60\31\2\u0140\u0141\5@!\2\u0141\u0142\5&\24\2\u0142"+
		"\u0151\3\2\2\2\u0143\u0144\5\60\31\2\u0144\u0145\5B\"\2\u0145\u0146\5"+
		"&\24\2\u0146\u0151\3\2\2\2\u0147\u0148\7\6\2\2\u0148\u0149\5&\24\2\u0149"+
		"\u014d\7\7\2\2\u014a\u014b\58\35\2\u014b\u014c\5&\24\2\u014c\u014e\3\2"+
		"\2\2\u014d\u014a\3\2\2\2\u014d\u014e\3\2\2\2\u014e\u0151\3\2\2\2\u014f"+
		"\u0151\5\60\31\2\u0150\u012d\3\2\2\2\u0150\u0137\3\2\2\2\u0150\u013f\3"+
		"\2\2\2\u0150\u0143\3\2\2\2\u0150\u0147\3\2\2\2\u0150\u014f\3\2\2\2\u0151"+
		"\'\3\2\2\2\u0152\u0153\5\62\32\2\u0153\u0154\5@!\2\u0154\u0155\5(\25\2"+
		"\u0155\u0164\3\2\2\2\u0156\u0157\5\62\32\2\u0157\u0158\5B\"\2\u0158\u0159"+
		"\5(\25\2\u0159\u0164\3\2\2\2\u015a\u015b\7\6\2\2\u015b\u015c\5(\25\2\u015c"+
		"\u0160\7\7\2\2\u015d\u015e\5:\36\2\u015e\u015f\5(\25\2\u015f\u0161\3\2"+
		"\2\2\u0160\u015d\3\2\2\2\u0160\u0161\3\2\2\2\u0161\u0164\3\2\2\2\u0162"+
		"\u0164\5\62\32\2\u0163\u0152\3\2\2\2\u0163\u0156\3\2\2\2\u0163\u015a\3"+
		"\2\2\2\u0163\u0162\3\2\2\2\u0164)\3\2\2\2\u0165\u0166\5\64\33\2\u0166"+
		"\u0167\5@!\2\u0167\u0168\5*\26\2\u0168\u0177\3\2\2\2\u0169\u016a\5\64"+
		"\33\2\u016a\u016b\5B\"\2\u016b\u016c\5*\26\2\u016c\u0177\3\2\2\2\u016d"+
		"\u016e\7\6\2\2\u016e\u016f\5*\26\2\u016f\u0173\7\7\2\2\u0170\u0171\5:"+
		"\36\2\u0171\u0172\5*\26\2\u0172\u0174\3\2\2\2\u0173\u0170\3\2\2\2\u0173"+
		"\u0174\3\2\2\2\u0174\u0177\3\2\2\2\u0175\u0177\5\64\33\2\u0176\u0165\3"+
		"\2\2\2\u0176\u0169\3\2\2\2\u0176\u016d\3\2\2\2\u0176\u0175\3\2\2\2\u0177"+
		"+\3\2\2\2\u0178\u0179\5\66\34\2\u0179\u017a\5@!\2\u017a\u017b\5,\27\2"+
		"\u017b\u018a\3\2\2\2\u017c\u017d\5\66\34\2\u017d\u017e\5B\"\2\u017e\u017f"+
		"\5,\27\2\u017f\u018a\3\2\2\2\u0180\u0181\7\6\2\2\u0181\u0182\5,\27\2\u0182"+
		"\u0186\7\7\2\2\u0183\u0184\5:\36\2\u0184\u0185\5,\27\2\u0185\u0187\3\2"+
		"\2\2\u0186\u0183\3\2\2\2\u0186\u0187\3\2\2\2\u0187\u018a\3\2\2\2\u0188"+
		"\u018a\5\66\34\2\u0189\u0178\3\2\2\2\u0189\u017c\3\2\2\2\u0189\u0180\3"+
		"\2\2\2\u0189\u0188\3\2\2\2\u018a-\3\2\2\2\u018b\u018d\5<\37\2\u018c\u018b"+
		"\3\2\2\2\u018d\u0190\3\2\2\2\u018e\u018c\3\2\2\2\u018e\u018f\3\2\2\2\u018f"+
		"\u0191\3\2\2\2\u0190\u018e\3\2\2\2\u0191\u0195\7*\2\2\u0192\u0195\5\16"+
		"\b\2\u0193\u0195\7/\2\2\u0194\u018e\3\2\2\2\u0194\u0192\3\2\2\2\u0194"+
		"\u0193\3\2\2\2\u0195/\3\2\2\2\u0196\u0198\5<\37\2\u0197\u0196\3\2\2\2"+
		"\u0198\u019b\3\2\2\2\u0199\u0197\3\2\2\2\u0199\u019a\3\2\2\2\u019a\u019c"+
		"\3\2\2\2\u019b\u0199\3\2\2\2\u019c\u01a7\7*\2\2\u019d\u019f\5<\37\2\u019e"+
		"\u019d\3\2\2\2\u019f\u01a2\3\2\2\2\u01a0\u019e\3\2\2\2\u01a0\u01a1\3\2"+
		"\2\2\u01a1\u01a3\3\2\2\2\u01a2\u01a0\3\2\2\2\u01a3\u01a7\7+\2\2\u01a4"+
		"\u01a7\5\16\b\2\u01a5\u01a7\7/\2\2\u01a6\u0199\3\2\2\2\u01a6\u01a0\3\2"+
		"\2\2\u01a6\u01a4\3\2\2\2\u01a6\u01a5\3\2\2\2\u01a7\61\3\2\2\2\u01a8\u01ac"+
		"\7,\2\2\u01a9\u01ac\7/\2\2\u01aa\u01ac\5\16\b\2\u01ab\u01a8\3\2\2\2\u01ab"+
		"\u01a9\3\2\2\2\u01ab\u01aa\3\2\2\2\u01ac\63\3\2\2\2\u01ad\u01b1\7-\2\2"+
		"\u01ae\u01b1\7/\2\2\u01af\u01b1\5\16\b\2\u01b0\u01ad\3\2\2\2\u01b0\u01ae"+
		"\3\2\2\2\u01b0\u01af\3\2\2\2\u01b1\65\3\2\2\2\u01b2\u01b6\5T+\2\u01b3"+
		"\u01b6\7/\2\2\u01b4\u01b6\5\16\b\2\u01b5\u01b2\3\2\2\2\u01b5\u01b3\3\2"+
		"\2\2\u01b5\u01b4\3\2\2\2\u01b6\67\3\2\2\2\u01b7\u01bc\5<\37\2\u01b8\u01bc"+
		"\5> \2\u01b9\u01bc\5@!\2\u01ba\u01bc\5B\"\2\u01bb\u01b7\3\2\2\2\u01bb"+
		"\u01b8\3\2\2\2\u01bb\u01b9\3\2\2\2\u01bb\u01ba\3\2\2\2\u01bc9\3\2\2\2"+
		"\u01bd\u01c0\5@!\2\u01be\u01c0\5B\"\2\u01bf\u01bd\3\2\2\2\u01bf\u01be"+
		"\3\2\2\2\u01c0;\3\2\2\2\u01c1\u01c2\t\2\2\2\u01c2=\3\2\2\2\u01c3\u01c4"+
		"\t\3\2\2\u01c4?\3\2\2\2\u01c5\u01c6\t\4\2\2\u01c6A\3\2\2\2\u01c7\u01c8"+
		"\t\5\2\2\u01c8C\3\2\2\2\u01c9\u01ca\7\13\2\2\u01ca\u01cb\7/\2\2\u01cb"+
		"\u01cc\7(\2\2\u01cc\u01cd\5N(\2\u01cd\u01ce\7)\2\2\u01ceE\3\2\2\2\u01cf"+
		"\u01d0\7\f\2\2\u01d0\u01d1\7/\2\2\u01d1\u01d2\7(\2\2\u01d2\u01d3\5N(\2"+
		"\u01d3\u01d4\7)\2\2\u01d4G\3\2\2\2\u01d5\u01d6\7\f\2\2\u01d6\u01d7\7/"+
		"\2\2\u01d7\u01d8\7(\2\2\u01d8\u01d9\5P)\2\u01d9\u01da\7)\2\2\u01daI\3"+
		"\2\2\2\u01db\u01dc\7/\2\2\u01dc\u01dd\7\'\2\2\u01dd\u01de\5R*\2\u01de"+
		"\u01df\7)\2\2\u01dfK\3\2\2\2\u01e0\u01e1\7/\2\2\u01e1\u01e2\7\t\2\2\u01e2"+
		"\u01e3\7/\2\2\u01e3\u01e4\7\'\2\2\u01e4\u01e5\5R*\2\u01e5\u01e6\7)\2\2"+
		"\u01e6M\3\2\2\2\u01e7\u01e8\7\22\2\2\u01e8\u01eb\7\'\2\2\u01e9\u01ec\7"+
		"*\2\2\u01ea\u01ec\5$\23\2\u01eb\u01e9\3\2\2\2\u01eb\u01ea\3\2\2\2\u01ec"+
		"\u0205\3\2\2\2\u01ed\u01ee\7\23\2\2\u01ee\u01f1\7\'\2\2\u01ef\u01f2\7"+
		"+\2\2\u01f0\u01f2\5&\24\2\u01f1\u01ef\3\2\2\2\u01f1\u01f0\3\2\2\2\u01f2"+
		"\u0205\3\2\2\2\u01f3\u01f4\7\24\2\2\u01f4\u01f5\7\'\2\2\u01f5\u0205\7"+
		",\2\2\u01f6\u01f7\7\25\2\2\u01f7\u01f8\7\'\2\2\u01f8\u0205\7-\2\2\u01f9"+
		"\u01fa\7\26\2\2\u01fa\u01fd\7\'\2\2\u01fb\u01fe\5T+\2\u01fc\u01fe\5,\27"+
		"\2\u01fd\u01fb\3\2\2\2\u01fd\u01fc\3\2\2\2\u01fe\u0205\3\2\2\2\u01ff\u0200"+
		"\7.\2\2\u0200\u0201\7\'\2\2\u0201\u0205\7/\2\2\u0202\u0205\7/\2\2\u0203"+
		"\u0205\5\16\b\2\u0204\u01e7\3\2\2\2\u0204\u01ed\3\2\2\2\u0204\u01f3\3"+
		"\2\2\2\u0204\u01f6\3\2\2\2\u0204\u01f9\3\2\2\2\u0204\u01ff\3\2\2\2\u0204"+
		"\u0202\3\2\2\2\u0204\u0203\3\2\2\2\u0205O\3\2\2\2\u0206\u0207\t\6\2\2"+
		"\u0207Q\3\2\2\2\u0208\u0212\7*\2\2\u0209\u0212\7+\2\2\u020a\u0212\7,\2"+
		"\2\u020b\u0212\7-\2\2\u020c\u0212\7\30\2\2\u020d\u0212\7\31\2\2\u020e"+
		"\u0212\7/\2\2\u020f\u0212\5\16\b\2\u0210\u0212\5\"\22\2\u0211\u0208\3"+
		"\2\2\2\u0211\u0209\3\2\2\2\u0211\u020a\3\2\2\2\u0211\u020b\3\2\2\2\u0211"+
		"\u020c\3\2\2\2\u0211\u020d\3\2\2\2\u0211\u020e\3\2\2\2\u0211\u020f\3\2"+
		"\2\2\u0211\u0210\3\2\2\2\u0212S\3\2\2\2\u0213\u0214\t\7\2\2\u0214U\3\2"+
		"\2\2\65Z`fo|\u0088\u0094\u009a\u009e\u00a8\u00ab\u00b7\u00bf\u00cc\u00d0"+
		"\u00da\u00de\u00e2\u00ec\u00f0\u00fc\u0106\u010d\u0116\u0128\u012b\u0132"+
		"\u013b\u014d\u0150\u0160\u0163\u0173\u0176\u0186\u0189\u018e\u0194\u0199"+
		"\u01a0\u01a6\u01ab\u01b0\u01b5\u01bb\u01bf\u01eb\u01f1\u01fd\u0204\u0211";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}