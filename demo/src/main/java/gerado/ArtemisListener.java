package gerado;// Generated from C:/Users/Guilherme Neves/Downloads/demo/src/main/java\Artemis.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ArtemisParser}.
 */
public interface ArtemisListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#artemis}.
	 * @param ctx the parse tree
	 */
	void enterArtemis(ArtemisParser.ArtemisContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#artemis}.
	 * @param ctx the parse tree
	 */
	void exitArtemis(ArtemisParser.ArtemisContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#importRule}.
	 * @param ctx the parse tree
	 */
	void enterImportRule(ArtemisParser.ImportRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#importRule}.
	 * @param ctx the parse tree
	 */
	void exitImportRule(ArtemisParser.ImportRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#externImport}.
	 * @param ctx the parse tree
	 */
	void enterExternImport(ArtemisParser.ExternImportContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#externImport}.
	 * @param ctx the parse tree
	 */
	void exitExternImport(ArtemisParser.ExternImportContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#struct}.
	 * @param ctx the parse tree
	 */
	void enterStruct(ArtemisParser.StructContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#struct}.
	 * @param ctx the parse tree
	 */
	void exitStruct(ArtemisParser.StructContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#mainFunction}.
	 * @param ctx the parse tree
	 */
	void enterMainFunction(ArtemisParser.MainFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#mainFunction}.
	 * @param ctx the parse tree
	 */
	void exitMainFunction(ArtemisParser.MainFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#functions}.
	 * @param ctx the parse tree
	 */
	void enterFunctions(ArtemisParser.FunctionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#functions}.
	 * @param ctx the parse tree
	 */
	void exitFunctions(ArtemisParser.FunctionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(ArtemisParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(ArtemisParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#parametersDefinition}.
	 * @param ctx the parse tree
	 */
	void enterParametersDefinition(ArtemisParser.ParametersDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#parametersDefinition}.
	 * @param ctx the parse tree
	 */
	void exitParametersDefinition(ArtemisParser.ParametersDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#parametersPassing}.
	 * @param ctx the parse tree
	 */
	void enterParametersPassing(ArtemisParser.ParametersPassingContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#parametersPassing}.
	 * @param ctx the parse tree
	 */
	void exitParametersPassing(ArtemisParser.ParametersPassingContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#returnRule}.
	 * @param ctx the parse tree
	 */
	void enterReturnRule(ArtemisParser.ReturnRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#returnRule}.
	 * @param ctx the parse tree
	 */
	void exitReturnRule(ArtemisParser.ReturnRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#escope}.
	 * @param ctx the parse tree
	 */
	void enterEscope(ArtemisParser.EscopeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#escope}.
	 * @param ctx the parse tree
	 */
	void exitEscope(ArtemisParser.EscopeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#conditionals}.
	 * @param ctx the parse tree
	 */
	void enterConditionals(ArtemisParser.ConditionalsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#conditionals}.
	 * @param ctx the parse tree
	 */
	void exitConditionals(ArtemisParser.ConditionalsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#ifRule}.
	 * @param ctx the parse tree
	 */
	void enterIfRule(ArtemisParser.IfRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#ifRule}.
	 * @param ctx the parse tree
	 */
	void exitIfRule(ArtemisParser.IfRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#elseIfRule}.
	 * @param ctx the parse tree
	 */
	void enterElseIfRule(ArtemisParser.ElseIfRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#elseIfRule}.
	 * @param ctx the parse tree
	 */
	void exitElseIfRule(ArtemisParser.ElseIfRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#elseRule}.
	 * @param ctx the parse tree
	 */
	void enterElseRule(ArtemisParser.ElseRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#elseRule}.
	 * @param ctx the parse tree
	 */
	void exitElseRule(ArtemisParser.ElseRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#whileLoop}.
	 * @param ctx the parse tree
	 */
	void enterWhileLoop(ArtemisParser.WhileLoopContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#whileLoop}.
	 * @param ctx the parse tree
	 */
	void exitWhileLoop(ArtemisParser.WhileLoopContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(ArtemisParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(ArtemisParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#intExpression}.
	 * @param ctx the parse tree
	 */
	void enterIntExpression(ArtemisParser.IntExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#intExpression}.
	 * @param ctx the parse tree
	 */
	void exitIntExpression(ArtemisParser.IntExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#floatExpression}.
	 * @param ctx the parse tree
	 */
	void enterFloatExpression(ArtemisParser.FloatExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#floatExpression}.
	 * @param ctx the parse tree
	 */
	void exitFloatExpression(ArtemisParser.FloatExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#charExpression}.
	 * @param ctx the parse tree
	 */
	void enterCharExpression(ArtemisParser.CharExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#charExpression}.
	 * @param ctx the parse tree
	 */
	void exitCharExpression(ArtemisParser.CharExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#stringExpression}.
	 * @param ctx the parse tree
	 */
	void enterStringExpression(ArtemisParser.StringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#stringExpression}.
	 * @param ctx the parse tree
	 */
	void exitStringExpression(ArtemisParser.StringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#booleanExpression}.
	 * @param ctx the parse tree
	 */
	void enterBooleanExpression(ArtemisParser.BooleanExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#booleanExpression}.
	 * @param ctx the parse tree
	 */
	void exitBooleanExpression(ArtemisParser.BooleanExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#intValues}.
	 * @param ctx the parse tree
	 */
	void enterIntValues(ArtemisParser.IntValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#intValues}.
	 * @param ctx the parse tree
	 */
	void exitIntValues(ArtemisParser.IntValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#floatValues}.
	 * @param ctx the parse tree
	 */
	void enterFloatValues(ArtemisParser.FloatValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#floatValues}.
	 * @param ctx the parse tree
	 */
	void exitFloatValues(ArtemisParser.FloatValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#charValues}.
	 * @param ctx the parse tree
	 */
	void enterCharValues(ArtemisParser.CharValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#charValues}.
	 * @param ctx the parse tree
	 */
	void exitCharValues(ArtemisParser.CharValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#stringValues}.
	 * @param ctx the parse tree
	 */
	void enterStringValues(ArtemisParser.StringValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#stringValues}.
	 * @param ctx the parse tree
	 */
	void exitStringValues(ArtemisParser.StringValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#booleanValuesExpression}.
	 * @param ctx the parse tree
	 */
	void enterBooleanValuesExpression(ArtemisParser.BooleanValuesExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#booleanValuesExpression}.
	 * @param ctx the parse tree
	 */
	void exitBooleanValuesExpression(ArtemisParser.BooleanValuesExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#allOperators}.
	 * @param ctx the parse tree
	 */
	void enterAllOperators(ArtemisParser.AllOperatorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#allOperators}.
	 * @param ctx the parse tree
	 */
	void exitAllOperators(ArtemisParser.AllOperatorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#logicRelationaOperators}.
	 * @param ctx the parse tree
	 */
	void enterLogicRelationaOperators(ArtemisParser.LogicRelationaOperatorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#logicRelationaOperators}.
	 * @param ctx the parse tree
	 */
	void exitLogicRelationaOperators(ArtemisParser.LogicRelationaOperatorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#sumOperators}.
	 * @param ctx the parse tree
	 */
	void enterSumOperators(ArtemisParser.SumOperatorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#sumOperators}.
	 * @param ctx the parse tree
	 */
	void exitSumOperators(ArtemisParser.SumOperatorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#multiplicationOperators}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicationOperators(ArtemisParser.MultiplicationOperatorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#multiplicationOperators}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicationOperators(ArtemisParser.MultiplicationOperatorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#logicOperators}.
	 * @param ctx the parse tree
	 */
	void enterLogicOperators(ArtemisParser.LogicOperatorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#logicOperators}.
	 * @param ctx the parse tree
	 */
	void exitLogicOperators(ArtemisParser.LogicOperatorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#relationalOperators}.
	 * @param ctx the parse tree
	 */
	void enterRelationalOperators(ArtemisParser.RelationalOperatorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#relationalOperators}.
	 * @param ctx the parse tree
	 */
	void exitRelationalOperators(ArtemisParser.RelationalOperatorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#globalVariable}.
	 * @param ctx the parse tree
	 */
	void enterGlobalVariable(ArtemisParser.GlobalVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#globalVariable}.
	 * @param ctx the parse tree
	 */
	void exitGlobalVariable(ArtemisParser.GlobalVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#localVariable}.
	 * @param ctx the parse tree
	 */
	void enterLocalVariable(ArtemisParser.LocalVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#localVariable}.
	 * @param ctx the parse tree
	 */
	void exitLocalVariable(ArtemisParser.LocalVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration(ArtemisParser.VariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration(ArtemisParser.VariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#variableAssigner}.
	 * @param ctx the parse tree
	 */
	void enterVariableAssigner(ArtemisParser.VariableAssignerContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#variableAssigner}.
	 * @param ctx the parse tree
	 */
	void exitVariableAssigner(ArtemisParser.VariableAssignerContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#operation}.
	 * @param ctx the parse tree
	 */
	void enterOperation(ArtemisParser.OperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#operation}.
	 * @param ctx the parse tree
	 */
	void exitOperation(ArtemisParser.OperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#structFieldAssigner}.
	 * @param ctx the parse tree
	 */
	void enterStructFieldAssigner(ArtemisParser.StructFieldAssignerContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#structFieldAssigner}.
	 * @param ctx the parse tree
	 */
	void exitStructFieldAssigner(ArtemisParser.StructFieldAssignerContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#typeAssignerValue}.
	 * @param ctx the parse tree
	 */
	void enterTypeAssignerValue(ArtemisParser.TypeAssignerValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#typeAssignerValue}.
	 * @param ctx the parse tree
	 */
	void exitTypeAssignerValue(ArtemisParser.TypeAssignerValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#types}.
	 * @param ctx the parse tree
	 */
	void enterTypes(ArtemisParser.TypesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#types}.
	 * @param ctx the parse tree
	 */
	void exitTypes(ArtemisParser.TypesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#values}.
	 * @param ctx the parse tree
	 */
	void enterValues(ArtemisParser.ValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#values}.
	 * @param ctx the parse tree
	 */
	void exitValues(ArtemisParser.ValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArtemisParser#booleanValues}.
	 * @param ctx the parse tree
	 */
	void enterBooleanValues(ArtemisParser.BooleanValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArtemisParser#booleanValues}.
	 * @param ctx the parse tree
	 */
	void exitBooleanValues(ArtemisParser.BooleanValuesContext ctx);
}