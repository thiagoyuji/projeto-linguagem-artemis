package gerado;// Generated from C:/Users/Guilherme Neves/Downloads/demo/src/main/java\Artemis.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ArtemisParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, IMPORT=8, GLOBAL=9, 
		VARIABLE=10, FUNCTION=11, RETURN=12, IF=13, ELSE=14, WHILE=15, INT=16, 
		FLOAT=17, CHAR=18, STRING=19, BOOLEAN=20, STRUCT=21, TRUE=22, FALSE=23, 
		ADD=24, SUB=25, MULT=26, DIV=27, REST=28, EQUAL=29, DIFFERENT=30, BIGGER_THAN=31, 
		LESS_THAN=32, BIGGER_EQUAL_THAN=33, LESS_EQUAL_THAN=34, AND=35, OR=36, 
		ASSIGNER=37, TYPE_DELIMITER=38, LINE_DELIMITER=39, NUMBERS=40, FLOATING_POINT=41, 
		CHARACTER=42, LITERAL_STRING=43, STRUCT_IDENTIFIER=44, IDENTIFIER=45, 
		COMMENT=46, LINE_COMMENT=47, WS=48;
	public static final int
		RULE_artemis = 0, RULE_importRule = 1, RULE_externImport = 2, RULE_struct = 3, 
		RULE_mainFunction = 4, RULE_functions = 5, RULE_functionCall = 6, RULE_parametersDefinition = 7, 
		RULE_parametersPassing = 8, RULE_returnRule = 9, RULE_escope = 10, RULE_conditionals = 11, 
		RULE_ifRule = 12, RULE_elseIfRule = 13, RULE_elseRule = 14, RULE_whileLoop = 15, 
		RULE_expression = 16, RULE_intExpression = 17, RULE_floatExpression = 18, 
		RULE_charExpression = 19, RULE_stringExpression = 20, RULE_booleanExpression = 21, 
		RULE_intValues = 22, RULE_floatValues = 23, RULE_charValues = 24, RULE_stringValues = 25, 
		RULE_booleanValuesExpression = 26, RULE_allOperators = 27, RULE_logicRelationaOperators = 28, 
		RULE_sumOperators = 29, RULE_multiplicationOperators = 30, RULE_logicOperators = 31, 
		RULE_relationalOperators = 32, RULE_globalVariable = 33, RULE_localVariable = 34, 
		RULE_variableDeclaration = 35, RULE_variableAssigner = 36, RULE_operation = 37, 
		RULE_structFieldAssigner = 38, RULE_typeAssignerValue = 39, RULE_types = 40, 
		RULE_values = 41, RULE_booleanValues = 42;
	private static String[] makeRuleNames() {
		return new String[] {
			"artemis", "importRule", "externImport", "struct", "mainFunction", "functions", 
			"functionCall", "parametersDefinition", "parametersPassing", "returnRule", 
			"escope", "conditionals", "ifRule", "elseIfRule", "elseRule", "whileLoop", 
			"expression", "intExpression", "floatExpression", "charExpression", "stringExpression", 
			"booleanExpression", "intValues", "floatValues", "charValues", "stringValues", 
			"booleanValuesExpression", "allOperators", "logicRelationaOperators", 
			"sumOperators", "multiplicationOperators", "logicOperators", "relationalOperators", 
			"globalVariable", "localVariable", "variableDeclaration", "variableAssigner", 
			"operation", "structFieldAssigner", "typeAssignerValue", "types", "values", 
			"booleanValues"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'{'", "'}'", "'()'", "'('", "')'", "','", "'.'", "'import'", "'global'", 
			"'variable'", "'function'", "'return'", "'if'", "'else'", "'while'", 
			"'int'", "'float'", "'char'", "'string'", "'boolean'", "'struct'", "'true'", 
			"'false'", "'+'", "'-'", "'*'", "'/'", "'%'", "'='", "'!='", "'>'", "'<'", 
			"'>='", "'<='", "'&&'", "'||'", "'<-'", "':'", "';'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, "IMPORT", "GLOBAL", "VARIABLE", 
			"FUNCTION", "RETURN", "IF", "ELSE", "WHILE", "INT", "FLOAT", "CHAR", 
			"STRING", "BOOLEAN", "STRUCT", "TRUE", "FALSE", "ADD", "SUB", "MULT", 
			"DIV", "REST", "EQUAL", "DIFFERENT", "BIGGER_THAN", "LESS_THAN", "BIGGER_EQUAL_THAN", 
			"LESS_EQUAL_THAN", "AND", "OR", "ASSIGNER", "TYPE_DELIMITER", "LINE_DELIMITER", 
			"NUMBERS", "FLOATING_POINT", "CHARACTER", "LITERAL_STRING", "STRUCT_IDENTIFIER", 
			"IDENTIFIER", "COMMENT", "LINE_COMMENT", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Artemis.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ArtemisParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ArtemisContext extends ParserRuleContext {
		public ImportRuleContext importRule() {
			return getRuleContext(ImportRuleContext.class,0);
		}
		public MainFunctionContext mainFunction() {
			return getRuleContext(MainFunctionContext.class,0);
		}
		public TerminalNode EOF() { return getToken(ArtemisParser.EOF, 0); }
		public List<StructContext> struct() {
			return getRuleContexts(StructContext.class);
		}
		public StructContext struct(int i) {
			return getRuleContext(StructContext.class,i);
		}
		public List<GlobalVariableContext> globalVariable() {
			return getRuleContexts(GlobalVariableContext.class);
		}
		public GlobalVariableContext globalVariable(int i) {
			return getRuleContext(GlobalVariableContext.class,i);
		}
		public List<FunctionsContext> functions() {
			return getRuleContexts(FunctionsContext.class);
		}
		public FunctionsContext functions(int i) {
			return getRuleContext(FunctionsContext.class,i);
		}
		public ArtemisContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_artemis; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterArtemis(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitArtemis(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitArtemis(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArtemisContext artemis() throws RecognitionException {
		ArtemisContext _localctx = new ArtemisContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_artemis);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(86);
			importRule();
			setState(90);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==STRUCT) {
				{
				{
				setState(87);
				struct();
				}
				}
				setState(92);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(96);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==GLOBAL) {
				{
				{
				setState(93);
				globalVariable();
				}
				}
				setState(98);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(102);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(99);
					functions();
					}
					} 
				}
				setState(104);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			setState(105);
			mainFunction();
			setState(106);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportRuleContext extends ParserRuleContext {
		public List<ExternImportContext> externImport() {
			return getRuleContexts(ExternImportContext.class);
		}
		public ExternImportContext externImport(int i) {
			return getRuleContext(ExternImportContext.class,i);
		}
		public ImportRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterImportRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitImportRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitImportRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImportRuleContext importRule() throws RecognitionException {
		ImportRuleContext _localctx = new ImportRuleContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_importRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==IMPORT) {
				{
				{
				setState(108);
				externImport();
				}
				}
				setState(113);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExternImportContext extends ParserRuleContext {
		public Token getImportName;
		public TerminalNode IMPORT() { return getToken(ArtemisParser.IMPORT, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public ExternImportContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_externImport; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterExternImport(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitExternImport(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitExternImport(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExternImportContext externImport() throws RecognitionException {
		ExternImportContext _localctx = new ExternImportContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_externImport);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114);
			match(IMPORT);
			setState(115);
			((ExternImportContext)_localctx).getImportName = match(IDENTIFIER);
			setState(116);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StructContext extends ParserRuleContext {
		public Token getStructIdentifier;
		public VariableDeclarationContext getVariableDeclaration;
		public TerminalNode STRUCT() { return getToken(ArtemisParser.STRUCT, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public TerminalNode STRUCT_IDENTIFIER() { return getToken(ArtemisParser.STRUCT_IDENTIFIER, 0); }
		public List<VariableDeclarationContext> variableDeclaration() {
			return getRuleContexts(VariableDeclarationContext.class);
		}
		public VariableDeclarationContext variableDeclaration(int i) {
			return getRuleContext(VariableDeclarationContext.class,i);
		}
		public StructContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_struct; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterStruct(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitStruct(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitStruct(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructContext struct() throws RecognitionException {
		StructContext _localctx = new StructContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_struct);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			match(STRUCT);
			setState(119);
			((StructContext)_localctx).getStructIdentifier = match(STRUCT_IDENTIFIER);
			setState(120);
			match(T__0);
			setState(122); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(121);
				((StructContext)_localctx).getVariableDeclaration = variableDeclaration();
				}
				}
				setState(124); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==VARIABLE );
			setState(126);
			match(T__1);
			setState(127);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainFunctionContext extends ParserRuleContext {
		public Token getFunctionName;
		public TerminalNode FUNCTION() { return getToken(ArtemisParser.FUNCTION, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public List<EscopeContext> escope() {
			return getRuleContexts(EscopeContext.class);
		}
		public EscopeContext escope(int i) {
			return getRuleContext(EscopeContext.class,i);
		}
		public MainFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mainFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterMainFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitMainFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitMainFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MainFunctionContext mainFunction() throws RecognitionException {
		MainFunctionContext _localctx = new MainFunctionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_mainFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(129);
			match(FUNCTION);
			setState(130);
			((MainFunctionContext)_localctx).getFunctionName = match(IDENTIFIER);
			setState(131);
			match(T__2);
			setState(132);
			match(T__0);
			setState(136);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARIABLE) | (1L << IF) | (1L << WHILE) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(133);
				escope();
				}
				}
				setState(138);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(139);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionsContext extends ParserRuleContext {
		public Token getFunctionName;
		public TypesContext getReturnType;
		public TerminalNode FUNCTION() { return getToken(ArtemisParser.FUNCTION, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public ParametersDefinitionContext parametersDefinition() {
			return getRuleContext(ParametersDefinitionContext.class,0);
		}
		public TerminalNode TYPE_DELIMITER() { return getToken(ArtemisParser.TYPE_DELIMITER, 0); }
		public List<EscopeContext> escope() {
			return getRuleContexts(EscopeContext.class);
		}
		public EscopeContext escope(int i) {
			return getRuleContext(EscopeContext.class,i);
		}
		public ReturnRuleContext returnRule() {
			return getRuleContext(ReturnRuleContext.class,0);
		}
		public TypesContext types() {
			return getRuleContext(TypesContext.class,0);
		}
		public FunctionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterFunctions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitFunctions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitFunctions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionsContext functions() throws RecognitionException {
		FunctionsContext _localctx = new FunctionsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_functions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(141);
			match(FUNCTION);
			setState(142);
			((FunctionsContext)_localctx).getFunctionName = match(IDENTIFIER);
			setState(143);
			match(T__3);
			setState(145);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(144);
				parametersDefinition();
				}
			}

			setState(147);
			match(T__4);
			setState(150);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==TYPE_DELIMITER) {
				{
				setState(148);
				match(TYPE_DELIMITER);
				setState(149);
				((FunctionsContext)_localctx).getReturnType = types();
				}
			}

			setState(152);
			match(T__0);
			setState(156);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARIABLE) | (1L << IF) | (1L << WHILE) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(153);
				escope();
				}
				}
				setState(158);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(160);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURN) {
				{
				setState(159);
				returnRule();
				}
			}

			setState(162);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public Token getFunctionName;
		public ParametersPassingContext getParameterPassing;
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public ParametersPassingContext parametersPassing() {
			return getRuleContext(ParametersPassingContext.class,0);
		}
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitFunctionCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitFunctionCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_functionCall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(164);
			((FunctionCallContext)_localctx).getFunctionName = match(IDENTIFIER);
			setState(171);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__3:
				{
				setState(165);
				match(T__3);
				setState(167);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << TRUE) | (1L << FALSE) | (1L << ADD) | (1L << SUB) | (1L << NUMBERS) | (1L << FLOATING_POINT) | (1L << CHARACTER) | (1L << LITERAL_STRING) | (1L << IDENTIFIER))) != 0)) {
					{
					setState(166);
					((FunctionCallContext)_localctx).getParameterPassing = parametersPassing();
					}
				}

				setState(169);
				match(T__4);
				}
				break;
			case T__2:
				{
				setState(170);
				match(T__2);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(174);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				{
				setState(173);
				match(LINE_DELIMITER);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersDefinitionContext extends ParserRuleContext {
		public Token getParameterName;
		public TypesContext getParameterType;
		public List<TerminalNode> TYPE_DELIMITER() { return getTokens(ArtemisParser.TYPE_DELIMITER); }
		public TerminalNode TYPE_DELIMITER(int i) {
			return getToken(ArtemisParser.TYPE_DELIMITER, i);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(ArtemisParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ArtemisParser.IDENTIFIER, i);
		}
		public List<TypesContext> types() {
			return getRuleContexts(TypesContext.class);
		}
		public TypesContext types(int i) {
			return getRuleContext(TypesContext.class,i);
		}
		public ParametersDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parametersDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterParametersDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitParametersDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitParametersDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParametersDefinitionContext parametersDefinition() throws RecognitionException {
		ParametersDefinitionContext _localctx = new ParametersDefinitionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_parametersDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(176);
			((ParametersDefinitionContext)_localctx).getParameterName = match(IDENTIFIER);
			setState(177);
			match(TYPE_DELIMITER);
			setState(178);
			((ParametersDefinitionContext)_localctx).getParameterType = types();
			}
			setState(186);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5) {
				{
				{
				setState(180);
				match(T__5);
				setState(181);
				((ParametersDefinitionContext)_localctx).getParameterName = match(IDENTIFIER);
				setState(182);
				match(TYPE_DELIMITER);
				setState(183);
				((ParametersDefinitionContext)_localctx).getParameterType = types();
				}
				}
				setState(188);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersPassingContext extends ParserRuleContext {
		public ValuesContext getValues;
		public List<ValuesContext> values() {
			return getRuleContexts(ValuesContext.class);
		}
		public ValuesContext values(int i) {
			return getRuleContext(ValuesContext.class,i);
		}
		public ParametersPassingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parametersPassing; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterParametersPassing(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitParametersPassing(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitParametersPassing(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParametersPassingContext parametersPassing() throws RecognitionException {
		ParametersPassingContext _localctx = new ParametersPassingContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_parametersPassing);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(189);
			((ParametersPassingContext)_localctx).getValues = values();
			setState(194);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5) {
				{
				{
				setState(190);
				match(T__5);
				setState(191);
				((ParametersPassingContext)_localctx).getValues = values();
				}
				}
				setState(196);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnRuleContext extends ParserRuleContext {
		public ValuesContext getValorRetorno;
		public TerminalNode RETURN() { return getToken(ArtemisParser.RETURN, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public ValuesContext values() {
			return getRuleContext(ValuesContext.class,0);
		}
		public ReturnRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterReturnRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitReturnRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitReturnRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnRuleContext returnRule() throws RecognitionException {
		ReturnRuleContext _localctx = new ReturnRuleContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_returnRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(197);
			match(RETURN);
			setState(198);
			((ReturnRuleContext)_localctx).getValorRetorno = values();
			setState(199);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EscopeContext extends ParserRuleContext {
		public OperationContext operation() {
			return getRuleContext(OperationContext.class,0);
		}
		public LocalVariableContext localVariable() {
			return getRuleContext(LocalVariableContext.class,0);
		}
		public VariableAssignerContext variableAssigner() {
			return getRuleContext(VariableAssignerContext.class,0);
		}
		public StructFieldAssignerContext structFieldAssigner() {
			return getRuleContext(StructFieldAssignerContext.class,0);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ConditionalsContext conditionals() {
			return getRuleContext(ConditionalsContext.class,0);
		}
		public WhileLoopContext whileLoop() {
			return getRuleContext(WhileLoopContext.class,0);
		}
		public EscopeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_escope; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterEscope(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitEscope(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitEscope(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EscopeContext escope() throws RecognitionException {
		EscopeContext _localctx = new EscopeContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_escope);
		try {
			setState(208);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(201);
				operation();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(202);
				localVariable();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(203);
				variableAssigner();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(204);
				structFieldAssigner();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(205);
				functionCall();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(206);
				conditionals();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(207);
				whileLoop();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionalsContext extends ParserRuleContext {
		public IfRuleContext ifRule() {
			return getRuleContext(IfRuleContext.class,0);
		}
		public ElseRuleContext elseRule() {
			return getRuleContext(ElseRuleContext.class,0);
		}
		public ConditionalsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterConditionals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitConditionals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitConditionals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionalsContext conditionals() throws RecognitionException {
		ConditionalsContext _localctx = new ConditionalsContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_conditionals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			ifRule();
			setState(212);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(211);
				elseRule();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfRuleContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(ArtemisParser.IF, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<EscopeContext> escope() {
			return getRuleContexts(EscopeContext.class);
		}
		public EscopeContext escope(int i) {
			return getRuleContext(EscopeContext.class,i);
		}
		public ReturnRuleContext returnRule() {
			return getRuleContext(ReturnRuleContext.class,0);
		}
		public ElseIfRuleContext elseIfRule() {
			return getRuleContext(ElseIfRuleContext.class,0);
		}
		public IfRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterIfRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitIfRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitIfRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfRuleContext ifRule() throws RecognitionException {
		IfRuleContext _localctx = new IfRuleContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_ifRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(214);
			match(IF);
			setState(215);
			match(T__3);
			setState(216);
			expression();
			setState(217);
			match(T__4);
			setState(218);
			match(T__0);
			setState(222);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARIABLE) | (1L << IF) | (1L << WHILE) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(219);
				escope();
				}
				}
				setState(224);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(226);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURN) {
				{
				setState(225);
				returnRule();
				}
			}

			setState(228);
			match(T__1);
			setState(230);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(229);
				elseIfRule();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseIfRuleContext extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(ArtemisParser.ELSE, 0); }
		public IfRuleContext ifRule() {
			return getRuleContext(IfRuleContext.class,0);
		}
		public ElseIfRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseIfRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterElseIfRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitElseIfRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitElseIfRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElseIfRuleContext elseIfRule() throws RecognitionException {
		ElseIfRuleContext _localctx = new ElseIfRuleContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_elseIfRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(232);
			match(ELSE);
			setState(233);
			ifRule();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseRuleContext extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(ArtemisParser.ELSE, 0); }
		public List<EscopeContext> escope() {
			return getRuleContexts(EscopeContext.class);
		}
		public EscopeContext escope(int i) {
			return getRuleContext(EscopeContext.class,i);
		}
		public ReturnRuleContext returnRule() {
			return getRuleContext(ReturnRuleContext.class,0);
		}
		public ElseRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterElseRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitElseRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitElseRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElseRuleContext elseRule() throws RecognitionException {
		ElseRuleContext _localctx = new ElseRuleContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_elseRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(235);
			match(ELSE);
			setState(236);
			match(T__0);
			setState(240);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARIABLE) | (1L << IF) | (1L << WHILE) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(237);
				escope();
				}
				}
				setState(242);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(244);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURN) {
				{
				setState(243);
				returnRule();
				}
			}

			setState(246);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileLoopContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(ArtemisParser.WHILE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<EscopeContext> escope() {
			return getRuleContexts(EscopeContext.class);
		}
		public EscopeContext escope(int i) {
			return getRuleContext(EscopeContext.class,i);
		}
		public WhileLoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileLoop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterWhileLoop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitWhileLoop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitWhileLoop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileLoopContext whileLoop() throws RecognitionException {
		WhileLoopContext _localctx = new WhileLoopContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_whileLoop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(248);
			match(WHILE);
			setState(249);
			match(T__3);
			setState(250);
			expression();
			setState(251);
			match(T__4);
			setState(252);
			match(T__0);
			setState(256);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << VARIABLE) | (1L << IF) | (1L << WHILE) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(253);
				escope();
				}
				}
				setState(258);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(259);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public IntExpressionContext intExpression() {
			return getRuleContext(IntExpressionContext.class,0);
		}
		public FloatExpressionContext floatExpression() {
			return getRuleContext(FloatExpressionContext.class,0);
		}
		public CharExpressionContext charExpression() {
			return getRuleContext(CharExpressionContext.class,0);
		}
		public StringExpressionContext stringExpression() {
			return getRuleContext(StringExpressionContext.class,0);
		}
		public BooleanExpressionContext booleanExpression() {
			return getRuleContext(BooleanExpressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_expression);
		try {
			setState(266);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(261);
				intExpression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(262);
				floatExpression();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(263);
				charExpression();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(264);
				stringExpression();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(265);
				booleanExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntExpressionContext extends ParserRuleContext {
		public IntValuesContext intValues() {
			return getRuleContext(IntValuesContext.class,0);
		}
		public MultiplicationOperatorsContext multiplicationOperators() {
			return getRuleContext(MultiplicationOperatorsContext.class,0);
		}
		public List<IntExpressionContext> intExpression() {
			return getRuleContexts(IntExpressionContext.class);
		}
		public IntExpressionContext intExpression(int i) {
			return getRuleContext(IntExpressionContext.class,i);
		}
		public List<SumOperatorsContext> sumOperators() {
			return getRuleContexts(SumOperatorsContext.class);
		}
		public SumOperatorsContext sumOperators(int i) {
			return getRuleContext(SumOperatorsContext.class,i);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public AllOperatorsContext allOperators() {
			return getRuleContext(AllOperatorsContext.class,0);
		}
		public IntExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterIntExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitIntExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitIntExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntExpressionContext intExpression() throws RecognitionException {
		IntExpressionContext _localctx = new IntExpressionContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_intExpression);
		int _la;
		try {
			int _alt;
			setState(303);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(268);
				intValues();
				setState(269);
				multiplicationOperators();
				setState(273);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
				while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(270);
						sumOperators();
						}
						} 
					}
					setState(275);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
				}
				setState(276);
				intExpression();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(278);
				intValues();
				setState(280); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(279);
						sumOperators();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(282); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
				} while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER );
				setState(284);
				intExpression();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(286);
				intValues();
				setState(287);
				logicOperators();
				setState(288);
				intExpression();
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(290);
				intValues();
				setState(291);
				relationalOperators();
				setState(292);
				intExpression();
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(294);
				match(T__3);
				setState(295);
				intExpression();
				setState(296);
				match(T__4);
				setState(300);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ADD) | (1L << SUB) | (1L << MULT) | (1L << DIV) | (1L << REST) | (1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN) | (1L << AND) | (1L << OR))) != 0)) {
					{
					setState(297);
					allOperators();
					setState(298);
					intExpression();
					}
				}

				}
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				{
				setState(302);
				intValues();
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FloatExpressionContext extends ParserRuleContext {
		public FloatValuesContext floatValues() {
			return getRuleContext(FloatValuesContext.class,0);
		}
		public MultiplicationOperatorsContext multiplicationOperators() {
			return getRuleContext(MultiplicationOperatorsContext.class,0);
		}
		public List<FloatExpressionContext> floatExpression() {
			return getRuleContexts(FloatExpressionContext.class);
		}
		public FloatExpressionContext floatExpression(int i) {
			return getRuleContext(FloatExpressionContext.class,i);
		}
		public List<SumOperatorsContext> sumOperators() {
			return getRuleContexts(SumOperatorsContext.class);
		}
		public SumOperatorsContext sumOperators(int i) {
			return getRuleContext(SumOperatorsContext.class,i);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public AllOperatorsContext allOperators() {
			return getRuleContext(AllOperatorsContext.class,0);
		}
		public FloatExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_floatExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterFloatExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitFloatExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitFloatExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FloatExpressionContext floatExpression() throws RecognitionException {
		FloatExpressionContext _localctx = new FloatExpressionContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_floatExpression);
		int _la;
		try {
			int _alt;
			setState(340);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(305);
				floatValues();
				setState(306);
				multiplicationOperators();
				setState(310);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
				while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(307);
						sumOperators();
						}
						} 
					}
					setState(312);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
				}
				setState(313);
				floatExpression();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(315);
				floatValues();
				setState(317); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(316);
						sumOperators();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(319); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
				} while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER );
				setState(321);
				floatExpression();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(323);
				floatValues();
				setState(324);
				logicOperators();
				setState(325);
				floatExpression();
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(327);
				floatValues();
				setState(328);
				relationalOperators();
				setState(329);
				floatExpression();
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(331);
				match(T__3);
				setState(332);
				floatExpression();
				setState(333);
				match(T__4);
				setState(337);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ADD) | (1L << SUB) | (1L << MULT) | (1L << DIV) | (1L << REST) | (1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN) | (1L << AND) | (1L << OR))) != 0)) {
					{
					setState(334);
					allOperators();
					setState(335);
					floatExpression();
					}
				}

				}
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				{
				setState(339);
				floatValues();
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharExpressionContext extends ParserRuleContext {
		public CharValuesContext charValues() {
			return getRuleContext(CharValuesContext.class,0);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public List<CharExpressionContext> charExpression() {
			return getRuleContexts(CharExpressionContext.class);
		}
		public CharExpressionContext charExpression(int i) {
			return getRuleContext(CharExpressionContext.class,i);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public LogicRelationaOperatorsContext logicRelationaOperators() {
			return getRuleContext(LogicRelationaOperatorsContext.class,0);
		}
		public CharExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_charExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterCharExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitCharExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitCharExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharExpressionContext charExpression() throws RecognitionException {
		CharExpressionContext _localctx = new CharExpressionContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_charExpression);
		int _la;
		try {
			setState(359);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(342);
				charValues();
				setState(343);
				logicOperators();
				setState(344);
				charExpression();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(346);
				charValues();
				setState(347);
				relationalOperators();
				setState(348);
				charExpression();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(350);
				match(T__3);
				setState(351);
				charExpression();
				setState(352);
				match(T__4);
				setState(356);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN) | (1L << AND) | (1L << OR))) != 0)) {
					{
					setState(353);
					logicRelationaOperators();
					setState(354);
					charExpression();
					}
				}

				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(358);
				charValues();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringExpressionContext extends ParserRuleContext {
		public StringValuesContext stringValues() {
			return getRuleContext(StringValuesContext.class,0);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public List<StringExpressionContext> stringExpression() {
			return getRuleContexts(StringExpressionContext.class);
		}
		public StringExpressionContext stringExpression(int i) {
			return getRuleContext(StringExpressionContext.class,i);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public LogicRelationaOperatorsContext logicRelationaOperators() {
			return getRuleContext(LogicRelationaOperatorsContext.class,0);
		}
		public StringExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterStringExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitStringExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitStringExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringExpressionContext stringExpression() throws RecognitionException {
		StringExpressionContext _localctx = new StringExpressionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_stringExpression);
		int _la;
		try {
			setState(378);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(361);
				stringValues();
				setState(362);
				logicOperators();
				setState(363);
				stringExpression();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(365);
				stringValues();
				setState(366);
				relationalOperators();
				setState(367);
				stringExpression();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(369);
				match(T__3);
				setState(370);
				stringExpression();
				setState(371);
				match(T__4);
				setState(375);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN) | (1L << AND) | (1L << OR))) != 0)) {
					{
					setState(372);
					logicRelationaOperators();
					setState(373);
					stringExpression();
					}
				}

				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(377);
				stringValues();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanExpressionContext extends ParserRuleContext {
		public BooleanValuesExpressionContext booleanValuesExpression() {
			return getRuleContext(BooleanValuesExpressionContext.class,0);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public List<BooleanExpressionContext> booleanExpression() {
			return getRuleContexts(BooleanExpressionContext.class);
		}
		public BooleanExpressionContext booleanExpression(int i) {
			return getRuleContext(BooleanExpressionContext.class,i);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public LogicRelationaOperatorsContext logicRelationaOperators() {
			return getRuleContext(LogicRelationaOperatorsContext.class,0);
		}
		public BooleanExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterBooleanExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitBooleanExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitBooleanExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanExpressionContext booleanExpression() throws RecognitionException {
		BooleanExpressionContext _localctx = new BooleanExpressionContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_booleanExpression);
		int _la;
		try {
			setState(397);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,37,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(380);
				booleanValuesExpression();
				setState(381);
				logicOperators();
				setState(382);
				booleanExpression();
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(384);
				booleanValuesExpression();
				setState(385);
				relationalOperators();
				setState(386);
				booleanExpression();
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(388);
				match(T__3);
				setState(389);
				booleanExpression();
				setState(390);
				match(T__4);
				setState(394);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN) | (1L << AND) | (1L << OR))) != 0)) {
					{
					setState(391);
					logicRelationaOperators();
					setState(392);
					booleanExpression();
					}
				}

				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(396);
				booleanValuesExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntValuesContext extends ParserRuleContext {
		public TerminalNode NUMBERS() { return getToken(ArtemisParser.NUMBERS, 0); }
		public List<SumOperatorsContext> sumOperators() {
			return getRuleContexts(SumOperatorsContext.class);
		}
		public SumOperatorsContext sumOperators(int i) {
			return getRuleContext(SumOperatorsContext.class,i);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public IntValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intValues; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterIntValues(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitIntValues(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitIntValues(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntValuesContext intValues() throws RecognitionException {
		IntValuesContext _localctx = new IntValuesContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_intValues);
		int _la;
		try {
			setState(408);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,39,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(402);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==ADD || _la==SUB) {
					{
					{
					setState(399);
					sumOperators();
					}
					}
					setState(404);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(405);
				match(NUMBERS);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(406);
				functionCall();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(407);
				match(IDENTIFIER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FloatValuesContext extends ParserRuleContext {
		public TerminalNode NUMBERS() { return getToken(ArtemisParser.NUMBERS, 0); }
		public List<SumOperatorsContext> sumOperators() {
			return getRuleContexts(SumOperatorsContext.class);
		}
		public SumOperatorsContext sumOperators(int i) {
			return getRuleContext(SumOperatorsContext.class,i);
		}
		public TerminalNode FLOATING_POINT() { return getToken(ArtemisParser.FLOATING_POINT, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FloatValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_floatValues; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterFloatValues(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitFloatValues(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitFloatValues(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FloatValuesContext floatValues() throws RecognitionException {
		FloatValuesContext _localctx = new FloatValuesContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_floatValues);
		int _la;
		try {
			setState(426);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(413);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==ADD || _la==SUB) {
					{
					{
					setState(410);
					sumOperators();
					}
					}
					setState(415);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(416);
				match(NUMBERS);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(420);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==ADD || _la==SUB) {
					{
					{
					setState(417);
					sumOperators();
					}
					}
					setState(422);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(423);
				match(FLOATING_POINT);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(424);
				functionCall();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(425);
				match(IDENTIFIER);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharValuesContext extends ParserRuleContext {
		public TerminalNode CHARACTER() { return getToken(ArtemisParser.CHARACTER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public CharValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_charValues; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterCharValues(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitCharValues(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitCharValues(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharValuesContext charValues() throws RecognitionException {
		CharValuesContext _localctx = new CharValuesContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_charValues);
		try {
			setState(431);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(428);
				match(CHARACTER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(429);
				match(IDENTIFIER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(430);
				functionCall();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringValuesContext extends ParserRuleContext {
		public TerminalNode LITERAL_STRING() { return getToken(ArtemisParser.LITERAL_STRING, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public StringValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringValues; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterStringValues(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitStringValues(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitStringValues(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringValuesContext stringValues() throws RecognitionException {
		StringValuesContext _localctx = new StringValuesContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_stringValues);
		try {
			setState(436);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,44,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(433);
				match(LITERAL_STRING);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(434);
				match(IDENTIFIER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(435);
				functionCall();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanValuesExpressionContext extends ParserRuleContext {
		public BooleanValuesContext booleanValues() {
			return getRuleContext(BooleanValuesContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public BooleanValuesExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanValuesExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterBooleanValuesExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitBooleanValuesExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitBooleanValuesExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanValuesExpressionContext booleanValuesExpression() throws RecognitionException {
		BooleanValuesExpressionContext _localctx = new BooleanValuesExpressionContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_booleanValuesExpression);
		try {
			setState(441);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,45,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(438);
				booleanValues();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(439);
				match(IDENTIFIER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(440);
				functionCall();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AllOperatorsContext extends ParserRuleContext {
		public SumOperatorsContext sumOperators() {
			return getRuleContext(SumOperatorsContext.class,0);
		}
		public MultiplicationOperatorsContext multiplicationOperators() {
			return getRuleContext(MultiplicationOperatorsContext.class,0);
		}
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public AllOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_allOperators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterAllOperators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitAllOperators(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitAllOperators(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AllOperatorsContext allOperators() throws RecognitionException {
		AllOperatorsContext _localctx = new AllOperatorsContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_allOperators);
		try {
			setState(447);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ADD:
			case SUB:
				enterOuterAlt(_localctx, 1);
				{
				setState(443);
				sumOperators();
				}
				break;
			case MULT:
			case DIV:
			case REST:
				enterOuterAlt(_localctx, 2);
				{
				setState(444);
				multiplicationOperators();
				}
				break;
			case AND:
			case OR:
				enterOuterAlt(_localctx, 3);
				{
				setState(445);
				logicOperators();
				}
				break;
			case EQUAL:
			case DIFFERENT:
			case BIGGER_THAN:
			case LESS_THAN:
			case BIGGER_EQUAL_THAN:
			case LESS_EQUAL_THAN:
				enterOuterAlt(_localctx, 4);
				{
				setState(446);
				relationalOperators();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicRelationaOperatorsContext extends ParserRuleContext {
		public LogicOperatorsContext logicOperators() {
			return getRuleContext(LogicOperatorsContext.class,0);
		}
		public RelationalOperatorsContext relationalOperators() {
			return getRuleContext(RelationalOperatorsContext.class,0);
		}
		public LogicRelationaOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicRelationaOperators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterLogicRelationaOperators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitLogicRelationaOperators(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitLogicRelationaOperators(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicRelationaOperatorsContext logicRelationaOperators() throws RecognitionException {
		LogicRelationaOperatorsContext _localctx = new LogicRelationaOperatorsContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_logicRelationaOperators);
		try {
			setState(451);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case AND:
			case OR:
				enterOuterAlt(_localctx, 1);
				{
				setState(449);
				logicOperators();
				}
				break;
			case EQUAL:
			case DIFFERENT:
			case BIGGER_THAN:
			case LESS_THAN:
			case BIGGER_EQUAL_THAN:
			case LESS_EQUAL_THAN:
				enterOuterAlt(_localctx, 2);
				{
				setState(450);
				relationalOperators();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SumOperatorsContext extends ParserRuleContext {
		public TerminalNode ADD() { return getToken(ArtemisParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(ArtemisParser.SUB, 0); }
		public SumOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sumOperators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterSumOperators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitSumOperators(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitSumOperators(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SumOperatorsContext sumOperators() throws RecognitionException {
		SumOperatorsContext _localctx = new SumOperatorsContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_sumOperators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(453);
			_la = _input.LA(1);
			if ( !(_la==ADD || _la==SUB) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplicationOperatorsContext extends ParserRuleContext {
		public TerminalNode MULT() { return getToken(ArtemisParser.MULT, 0); }
		public TerminalNode DIV() { return getToken(ArtemisParser.DIV, 0); }
		public TerminalNode REST() { return getToken(ArtemisParser.REST, 0); }
		public MultiplicationOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicationOperators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterMultiplicationOperators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitMultiplicationOperators(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitMultiplicationOperators(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiplicationOperatorsContext multiplicationOperators() throws RecognitionException {
		MultiplicationOperatorsContext _localctx = new MultiplicationOperatorsContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_multiplicationOperators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(455);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MULT) | (1L << DIV) | (1L << REST))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicOperatorsContext extends ParserRuleContext {
		public TerminalNode AND() { return getToken(ArtemisParser.AND, 0); }
		public TerminalNode OR() { return getToken(ArtemisParser.OR, 0); }
		public LogicOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicOperators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterLogicOperators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitLogicOperators(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitLogicOperators(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicOperatorsContext logicOperators() throws RecognitionException {
		LogicOperatorsContext _localctx = new LogicOperatorsContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_logicOperators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(457);
			_la = _input.LA(1);
			if ( !(_la==AND || _la==OR) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalOperatorsContext extends ParserRuleContext {
		public TerminalNode EQUAL() { return getToken(ArtemisParser.EQUAL, 0); }
		public TerminalNode DIFFERENT() { return getToken(ArtemisParser.DIFFERENT, 0); }
		public TerminalNode BIGGER_THAN() { return getToken(ArtemisParser.BIGGER_THAN, 0); }
		public TerminalNode BIGGER_EQUAL_THAN() { return getToken(ArtemisParser.BIGGER_EQUAL_THAN, 0); }
		public TerminalNode LESS_THAN() { return getToken(ArtemisParser.LESS_THAN, 0); }
		public TerminalNode LESS_EQUAL_THAN() { return getToken(ArtemisParser.LESS_EQUAL_THAN, 0); }
		public RelationalOperatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalOperators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterRelationalOperators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitRelationalOperators(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitRelationalOperators(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelationalOperatorsContext relationalOperators() throws RecognitionException {
		RelationalOperatorsContext _localctx = new RelationalOperatorsContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_relationalOperators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(459);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << DIFFERENT) | (1L << BIGGER_THAN) | (1L << LESS_THAN) | (1L << BIGGER_EQUAL_THAN) | (1L << LESS_EQUAL_THAN))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GlobalVariableContext extends ParserRuleContext {
		public Token getGlobalVariableName;
		public TypeAssignerValueContext getTypeAssignerValue;
		public TerminalNode GLOBAL() { return getToken(ArtemisParser.GLOBAL, 0); }
		public TerminalNode TYPE_DELIMITER() { return getToken(ArtemisParser.TYPE_DELIMITER, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public TypeAssignerValueContext typeAssignerValue() {
			return getRuleContext(TypeAssignerValueContext.class,0);
		}
		public GlobalVariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_globalVariable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterGlobalVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitGlobalVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitGlobalVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GlobalVariableContext globalVariable() throws RecognitionException {
		GlobalVariableContext _localctx = new GlobalVariableContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_globalVariable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(461);
			match(GLOBAL);
			setState(462);
			((GlobalVariableContext)_localctx).getGlobalVariableName = match(IDENTIFIER);
			setState(463);
			match(TYPE_DELIMITER);
			setState(464);
			((GlobalVariableContext)_localctx).getTypeAssignerValue = typeAssignerValue();
			setState(465);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LocalVariableContext extends ParserRuleContext {
		public Token getLocalVariableName;
		public TypeAssignerValueContext getTypeAssignerValue;
		public TerminalNode VARIABLE() { return getToken(ArtemisParser.VARIABLE, 0); }
		public TerminalNode TYPE_DELIMITER() { return getToken(ArtemisParser.TYPE_DELIMITER, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public TypeAssignerValueContext typeAssignerValue() {
			return getRuleContext(TypeAssignerValueContext.class,0);
		}
		public LocalVariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_localVariable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterLocalVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitLocalVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitLocalVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LocalVariableContext localVariable() throws RecognitionException {
		LocalVariableContext _localctx = new LocalVariableContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_localVariable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(467);
			match(VARIABLE);
			setState(468);
			((LocalVariableContext)_localctx).getLocalVariableName = match(IDENTIFIER);
			setState(469);
			match(TYPE_DELIMITER);
			setState(470);
			((LocalVariableContext)_localctx).getTypeAssignerValue = typeAssignerValue();
			setState(471);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclarationContext extends ParserRuleContext {
		public Token getLocalVariableName;
		public TypesContext getVariableTypes;
		public TerminalNode VARIABLE() { return getToken(ArtemisParser.VARIABLE, 0); }
		public TerminalNode TYPE_DELIMITER() { return getToken(ArtemisParser.TYPE_DELIMITER, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public TypesContext types() {
			return getRuleContext(TypesContext.class,0);
		}
		public VariableDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterVariableDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitVariableDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitVariableDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableDeclarationContext variableDeclaration() throws RecognitionException {
		VariableDeclarationContext _localctx = new VariableDeclarationContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_variableDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(473);
			match(VARIABLE);
			setState(474);
			((VariableDeclarationContext)_localctx).getLocalVariableName = match(IDENTIFIER);
			setState(475);
			match(TYPE_DELIMITER);
			setState(476);
			((VariableDeclarationContext)_localctx).getVariableTypes = types();
			setState(477);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableAssignerContext extends ParserRuleContext {
		public Token getVariableName;
		public ValuesContext getValues;
		public TerminalNode ASSIGNER() { return getToken(ArtemisParser.ASSIGNER, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public ValuesContext values() {
			return getRuleContext(ValuesContext.class,0);
		}
		public VariableAssignerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableAssigner; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterVariableAssigner(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitVariableAssigner(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitVariableAssigner(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableAssignerContext variableAssigner() throws RecognitionException {
		VariableAssignerContext _localctx = new VariableAssignerContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_variableAssigner);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(479);
			((VariableAssignerContext)_localctx).getVariableName = match(IDENTIFIER);
			setState(480);
			match(ASSIGNER);
			setState(481);
			((VariableAssignerContext)_localctx).getValues = values();
			setState(482);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperationContext extends ParserRuleContext {
		public Token getVariableDestName;
		public Token getOpt1Name;
		public AllOperatorsContext getOperator;
		public Token getOpt2Name;
		public TerminalNode ASSIGNER() { return getToken(ArtemisParser.ASSIGNER, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(ArtemisParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ArtemisParser.IDENTIFIER, i);
		}
		public AllOperatorsContext allOperators() {
			return getRuleContext(AllOperatorsContext.class,0);
		}
		public OperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterOperation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitOperation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitOperation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperationContext operation() throws RecognitionException {
		OperationContext _localctx = new OperationContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_operation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(484);
			((OperationContext)_localctx).getVariableDestName = match(IDENTIFIER);
			setState(485);
			match(ASSIGNER);
			setState(486);
			match(T__3);
			setState(487);
			((OperationContext)_localctx).getOpt1Name = match(IDENTIFIER);
			setState(488);
			((OperationContext)_localctx).getOperator = allOperators();
			setState(489);
			((OperationContext)_localctx).getOpt2Name = match(IDENTIFIER);
			setState(490);
			match(T__4);
			setState(491);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StructFieldAssignerContext extends ParserRuleContext {
		public Token getVariableName;
		public Token getVariableField;
		public ValuesContext getValues;
		public TerminalNode ASSIGNER() { return getToken(ArtemisParser.ASSIGNER, 0); }
		public TerminalNode LINE_DELIMITER() { return getToken(ArtemisParser.LINE_DELIMITER, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(ArtemisParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ArtemisParser.IDENTIFIER, i);
		}
		public ValuesContext values() {
			return getRuleContext(ValuesContext.class,0);
		}
		public StructFieldAssignerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structFieldAssigner; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterStructFieldAssigner(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitStructFieldAssigner(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitStructFieldAssigner(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StructFieldAssignerContext structFieldAssigner() throws RecognitionException {
		StructFieldAssignerContext _localctx = new StructFieldAssignerContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_structFieldAssigner);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(493);
			((StructFieldAssignerContext)_localctx).getVariableName = match(IDENTIFIER);
			setState(494);
			match(T__6);
			setState(495);
			((StructFieldAssignerContext)_localctx).getVariableField = match(IDENTIFIER);
			setState(496);
			match(ASSIGNER);
			setState(497);
			((StructFieldAssignerContext)_localctx).getValues = values();
			setState(498);
			match(LINE_DELIMITER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeAssignerValueContext extends ParserRuleContext {
		public Token getInt;
		public Token getNumber;
		public IntExpressionContext getIntExpression;
		public Token getFloat;
		public Token getFloatingPoint;
		public FloatExpressionContext getFloatExpression;
		public Token getChar;
		public Token getCharacter;
		public Token getString;
		public Token getLiteralString;
		public Token getBoolean;
		public BooleanValuesContext getBooleanValues;
		public BooleanExpressionContext getBooleanExpression;
		public Token getStructIdentifier;
		public Token getIdentifier;
		public FunctionCallContext getFunctionCall;
		public TerminalNode ASSIGNER() { return getToken(ArtemisParser.ASSIGNER, 0); }
		public TerminalNode INT() { return getToken(ArtemisParser.INT, 0); }
		public TerminalNode NUMBERS() { return getToken(ArtemisParser.NUMBERS, 0); }
		public IntExpressionContext intExpression() {
			return getRuleContext(IntExpressionContext.class,0);
		}
		public TerminalNode FLOAT() { return getToken(ArtemisParser.FLOAT, 0); }
		public TerminalNode FLOATING_POINT() { return getToken(ArtemisParser.FLOATING_POINT, 0); }
		public FloatExpressionContext floatExpression() {
			return getRuleContext(FloatExpressionContext.class,0);
		}
		public TerminalNode CHAR() { return getToken(ArtemisParser.CHAR, 0); }
		public TerminalNode CHARACTER() { return getToken(ArtemisParser.CHARACTER, 0); }
		public TerminalNode STRING() { return getToken(ArtemisParser.STRING, 0); }
		public TerminalNode LITERAL_STRING() { return getToken(ArtemisParser.LITERAL_STRING, 0); }
		public TerminalNode BOOLEAN() { return getToken(ArtemisParser.BOOLEAN, 0); }
		public BooleanValuesContext booleanValues() {
			return getRuleContext(BooleanValuesContext.class,0);
		}
		public BooleanExpressionContext booleanExpression() {
			return getRuleContext(BooleanExpressionContext.class,0);
		}
		public TerminalNode STRUCT_IDENTIFIER() { return getToken(ArtemisParser.STRUCT_IDENTIFIER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public TypeAssignerValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeAssignerValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterTypeAssignerValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitTypeAssignerValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitTypeAssignerValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeAssignerValueContext typeAssignerValue() throws RecognitionException {
		TypeAssignerValueContext _localctx = new TypeAssignerValueContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_typeAssignerValue);
		try {
			setState(529);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(500);
				((TypeAssignerValueContext)_localctx).getInt = match(INT);
				setState(501);
				match(ASSIGNER);
				setState(504);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,48,_ctx) ) {
				case 1:
					{
					setState(502);
					((TypeAssignerValueContext)_localctx).getNumber = match(NUMBERS);
					}
					break;
				case 2:
					{
					setState(503);
					((TypeAssignerValueContext)_localctx).getIntExpression = intExpression();
					}
					break;
				}
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(506);
				((TypeAssignerValueContext)_localctx).getFloat = match(FLOAT);
				setState(507);
				match(ASSIGNER);
				setState(510);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,49,_ctx) ) {
				case 1:
					{
					setState(508);
					((TypeAssignerValueContext)_localctx).getFloatingPoint = match(FLOATING_POINT);
					}
					break;
				case 2:
					{
					setState(509);
					((TypeAssignerValueContext)_localctx).getFloatExpression = floatExpression();
					}
					break;
				}
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(512);
				((TypeAssignerValueContext)_localctx).getChar = match(CHAR);
				setState(513);
				match(ASSIGNER);
				setState(514);
				((TypeAssignerValueContext)_localctx).getCharacter = match(CHARACTER);
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(515);
				((TypeAssignerValueContext)_localctx).getString = match(STRING);
				setState(516);
				match(ASSIGNER);
				setState(517);
				((TypeAssignerValueContext)_localctx).getLiteralString = match(LITERAL_STRING);
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(518);
				((TypeAssignerValueContext)_localctx).getBoolean = match(BOOLEAN);
				setState(519);
				match(ASSIGNER);
				setState(522);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,50,_ctx) ) {
				case 1:
					{
					setState(520);
					((TypeAssignerValueContext)_localctx).getBooleanValues = booleanValues();
					}
					break;
				case 2:
					{
					setState(521);
					((TypeAssignerValueContext)_localctx).getBooleanExpression = booleanExpression();
					}
					break;
				}
				}
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				{
				setState(524);
				((TypeAssignerValueContext)_localctx).getStructIdentifier = match(STRUCT_IDENTIFIER);
				setState(525);
				match(ASSIGNER);
				setState(526);
				((TypeAssignerValueContext)_localctx).getIdentifier = match(IDENTIFIER);
				}
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(527);
				((TypeAssignerValueContext)_localctx).getIdentifier = match(IDENTIFIER);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(528);
				((TypeAssignerValueContext)_localctx).getFunctionCall = functionCall();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypesContext extends ParserRuleContext {
		public Token getInt;
		public Token getFloat;
		public Token getChar;
		public Token getString;
		public Token getBoolean;
		public Token getStructIdentifier;
		public TerminalNode INT() { return getToken(ArtemisParser.INT, 0); }
		public TerminalNode FLOAT() { return getToken(ArtemisParser.FLOAT, 0); }
		public TerminalNode CHAR() { return getToken(ArtemisParser.CHAR, 0); }
		public TerminalNode STRING() { return getToken(ArtemisParser.STRING, 0); }
		public TerminalNode BOOLEAN() { return getToken(ArtemisParser.BOOLEAN, 0); }
		public TerminalNode STRUCT_IDENTIFIER() { return getToken(ArtemisParser.STRUCT_IDENTIFIER, 0); }
		public TypesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_types; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterTypes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitTypes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitTypes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypesContext types() throws RecognitionException {
		TypesContext _localctx = new TypesContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_types);
		try {
			setState(537);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(531);
				((TypesContext)_localctx).getInt = match(INT);
				}
				break;
			case FLOAT:
				enterOuterAlt(_localctx, 2);
				{
				setState(532);
				((TypesContext)_localctx).getFloat = match(FLOAT);
				}
				break;
			case CHAR:
				enterOuterAlt(_localctx, 3);
				{
				setState(533);
				((TypesContext)_localctx).getChar = match(CHAR);
				}
				break;
			case STRING:
				enterOuterAlt(_localctx, 4);
				{
				setState(534);
				((TypesContext)_localctx).getString = match(STRING);
				}
				break;
			case BOOLEAN:
				enterOuterAlt(_localctx, 5);
				{
				setState(535);
				((TypesContext)_localctx).getBoolean = match(BOOLEAN);
				}
				break;
			case STRUCT_IDENTIFIER:
				enterOuterAlt(_localctx, 6);
				{
				setState(536);
				((TypesContext)_localctx).getStructIdentifier = match(STRUCT_IDENTIFIER);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValuesContext extends ParserRuleContext {
		public Token getNumber;
		public Token getFloatingPoint;
		public Token getCharacter;
		public Token getLiteralString;
		public Token getTrue;
		public Token getFalse;
		public Token getIdentifier;
		public FunctionCallContext getFunctionCall;
		public ExpressionContext getExpression;
		public TerminalNode NUMBERS() { return getToken(ArtemisParser.NUMBERS, 0); }
		public TerminalNode FLOATING_POINT() { return getToken(ArtemisParser.FLOATING_POINT, 0); }
		public TerminalNode CHARACTER() { return getToken(ArtemisParser.CHARACTER, 0); }
		public TerminalNode LITERAL_STRING() { return getToken(ArtemisParser.LITERAL_STRING, 0); }
		public TerminalNode TRUE() { return getToken(ArtemisParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(ArtemisParser.FALSE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ArtemisParser.IDENTIFIER, 0); }
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_values; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterValues(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitValues(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitValues(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValuesContext values() throws RecognitionException {
		ValuesContext _localctx = new ValuesContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_values);
		try {
			setState(548);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(539);
				((ValuesContext)_localctx).getNumber = match(NUMBERS);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(540);
				((ValuesContext)_localctx).getFloatingPoint = match(FLOATING_POINT);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(541);
				((ValuesContext)_localctx).getCharacter = match(CHARACTER);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(542);
				((ValuesContext)_localctx).getLiteralString = match(LITERAL_STRING);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(543);
				((ValuesContext)_localctx).getTrue = match(TRUE);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(544);
				((ValuesContext)_localctx).getFalse = match(FALSE);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(545);
				((ValuesContext)_localctx).getIdentifier = match(IDENTIFIER);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(546);
				((ValuesContext)_localctx).getFunctionCall = functionCall();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(547);
				((ValuesContext)_localctx).getExpression = expression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanValuesContext extends ParserRuleContext {
		public Token getTrue;
		public Token getFalse;
		public TerminalNode TRUE() { return getToken(ArtemisParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(ArtemisParser.FALSE, 0); }
		public BooleanValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanValues; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).enterBooleanValues(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ArtemisListener ) ((ArtemisListener)listener).exitBooleanValues(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ArtemisVisitor ) return ((ArtemisVisitor<? extends T>)visitor).visitBooleanValues(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanValuesContext booleanValues() throws RecognitionException {
		BooleanValuesContext _localctx = new BooleanValuesContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_booleanValues);
		try {
			setState(552);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUE:
				enterOuterAlt(_localctx, 1);
				{
				setState(550);
				((BooleanValuesContext)_localctx).getTrue = match(TRUE);
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 2);
				{
				setState(551);
				((BooleanValuesContext)_localctx).getFalse = match(FALSE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\62\u022d\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\3\2\3\2\7\2[\n\2\f\2\16\2^\13\2\3\2\7\2a\n\2\f\2\16\2d\13\2\3\2\7"+
		"\2g\n\2\f\2\16\2j\13\2\3\2\3\2\3\2\3\3\7\3p\n\3\f\3\16\3s\13\3\3\4\3\4"+
		"\3\4\3\4\3\5\3\5\3\5\3\5\6\5}\n\5\r\5\16\5~\3\5\3\5\3\5\3\6\3\6\3\6\3"+
		"\6\3\6\7\6\u0089\n\6\f\6\16\6\u008c\13\6\3\6\3\6\3\7\3\7\3\7\3\7\5\7\u0094"+
		"\n\7\3\7\3\7\3\7\5\7\u0099\n\7\3\7\3\7\7\7\u009d\n\7\f\7\16\7\u00a0\13"+
		"\7\3\7\5\7\u00a3\n\7\3\7\3\7\3\b\3\b\3\b\5\b\u00aa\n\b\3\b\3\b\5\b\u00ae"+
		"\n\b\3\b\5\b\u00b1\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u00bb\n\t\f"+
		"\t\16\t\u00be\13\t\3\n\3\n\3\n\7\n\u00c3\n\n\f\n\16\n\u00c6\13\n\3\13"+
		"\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00d3\n\f\3\r\3\r\5\r"+
		"\u00d7\n\r\3\16\3\16\3\16\3\16\3\16\3\16\7\16\u00df\n\16\f\16\16\16\u00e2"+
		"\13\16\3\16\5\16\u00e5\n\16\3\16\3\16\5\16\u00e9\n\16\3\17\3\17\3\17\3"+
		"\20\3\20\3\20\7\20\u00f1\n\20\f\20\16\20\u00f4\13\20\3\20\5\20\u00f7\n"+
		"\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\7\21\u0101\n\21\f\21\16\21"+
		"\u0104\13\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\5\22\u010d\n\22\3\23\3"+
		"\23\3\23\7\23\u0112\n\23\f\23\16\23\u0115\13\23\3\23\3\23\3\23\3\23\6"+
		"\23\u011b\n\23\r\23\16\23\u011c\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\5\23\u012f\n\23\3\23\5\23\u0132"+
		"\n\23\3\24\3\24\3\24\7\24\u0137\n\24\f\24\16\24\u013a\13\24\3\24\3\24"+
		"\3\24\3\24\6\24\u0140\n\24\r\24\16\24\u0141\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u0154\n\24"+
		"\3\24\5\24\u0157\n\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\5\25\u0167\n\25\3\25\5\25\u016a\n\25\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u017a"+
		"\n\26\3\26\5\26\u017d\n\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\5\27\u018d\n\27\3\27\5\27\u0190\n\27\3\30\7"+
		"\30\u0193\n\30\f\30\16\30\u0196\13\30\3\30\3\30\3\30\5\30\u019b\n\30\3"+
		"\31\7\31\u019e\n\31\f\31\16\31\u01a1\13\31\3\31\3\31\7\31\u01a5\n\31\f"+
		"\31\16\31\u01a8\13\31\3\31\3\31\3\31\5\31\u01ad\n\31\3\32\3\32\3\32\5"+
		"\32\u01b2\n\32\3\33\3\33\3\33\5\33\u01b7\n\33\3\34\3\34\3\34\5\34\u01bc"+
		"\n\34\3\35\3\35\3\35\3\35\5\35\u01c2\n\35\3\36\3\36\5\36\u01c6\n\36\3"+
		"\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3%\3"+
		"%\3%\3%\3%\3%\3&\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3(\3"+
		"(\3(\3(\3(\3(\3(\3)\3)\3)\3)\5)\u01fb\n)\3)\3)\3)\3)\5)\u0201\n)\3)\3"+
		")\3)\3)\3)\3)\3)\3)\3)\3)\5)\u020d\n)\3)\3)\3)\3)\3)\5)\u0214\n)\3*\3"+
		"*\3*\3*\3*\3*\5*\u021c\n*\3+\3+\3+\3+\3+\3+\3+\3+\3+\5+\u0227\n+\3,\3"+
		",\5,\u022b\n,\3,\2\2-\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,."+
		"\60\62\64\668:<>@BDFHJLNPRTV\2\6\3\2\32\33\3\2\34\36\3\2%&\3\2\37$\2\u0267"+
		"\2X\3\2\2\2\4q\3\2\2\2\6t\3\2\2\2\bx\3\2\2\2\n\u0083\3\2\2\2\f\u008f\3"+
		"\2\2\2\16\u00a6\3\2\2\2\20\u00b2\3\2\2\2\22\u00bf\3\2\2\2\24\u00c7\3\2"+
		"\2\2\26\u00d2\3\2\2\2\30\u00d4\3\2\2\2\32\u00d8\3\2\2\2\34\u00ea\3\2\2"+
		"\2\36\u00ed\3\2\2\2 \u00fa\3\2\2\2\"\u010c\3\2\2\2$\u0131\3\2\2\2&\u0156"+
		"\3\2\2\2(\u0169\3\2\2\2*\u017c\3\2\2\2,\u018f\3\2\2\2.\u019a\3\2\2\2\60"+
		"\u01ac\3\2\2\2\62\u01b1\3\2\2\2\64\u01b6\3\2\2\2\66\u01bb\3\2\2\28\u01c1"+
		"\3\2\2\2:\u01c5\3\2\2\2<\u01c7\3\2\2\2>\u01c9\3\2\2\2@\u01cb\3\2\2\2B"+
		"\u01cd\3\2\2\2D\u01cf\3\2\2\2F\u01d5\3\2\2\2H\u01db\3\2\2\2J\u01e1\3\2"+
		"\2\2L\u01e6\3\2\2\2N\u01ef\3\2\2\2P\u0213\3\2\2\2R\u021b\3\2\2\2T\u0226"+
		"\3\2\2\2V\u022a\3\2\2\2X\\\5\4\3\2Y[\5\b\5\2ZY\3\2\2\2[^\3\2\2\2\\Z\3"+
		"\2\2\2\\]\3\2\2\2]b\3\2\2\2^\\\3\2\2\2_a\5D#\2`_\3\2\2\2ad\3\2\2\2b`\3"+
		"\2\2\2bc\3\2\2\2ch\3\2\2\2db\3\2\2\2eg\5\f\7\2fe\3\2\2\2gj\3\2\2\2hf\3"+
		"\2\2\2hi\3\2\2\2ik\3\2\2\2jh\3\2\2\2kl\5\n\6\2lm\7\2\2\3m\3\3\2\2\2np"+
		"\5\6\4\2on\3\2\2\2ps\3\2\2\2qo\3\2\2\2qr\3\2\2\2r\5\3\2\2\2sq\3\2\2\2"+
		"tu\7\n\2\2uv\7/\2\2vw\7)\2\2w\7\3\2\2\2xy\7\27\2\2yz\7.\2\2z|\7\3\2\2"+
		"{}\5H%\2|{\3\2\2\2}~\3\2\2\2~|\3\2\2\2~\177\3\2\2\2\177\u0080\3\2\2\2"+
		"\u0080\u0081\7\4\2\2\u0081\u0082\7)\2\2\u0082\t\3\2\2\2\u0083\u0084\7"+
		"\r\2\2\u0084\u0085\7/\2\2\u0085\u0086\7\5\2\2\u0086\u008a\7\3\2\2\u0087"+
		"\u0089\5\26\f\2\u0088\u0087\3\2\2\2\u0089\u008c\3\2\2\2\u008a\u0088\3"+
		"\2\2\2\u008a\u008b\3\2\2\2\u008b\u008d\3\2\2\2\u008c\u008a\3\2\2\2\u008d"+
		"\u008e\7\4\2\2\u008e\13\3\2\2\2\u008f\u0090\7\r\2\2\u0090\u0091\7/\2\2"+
		"\u0091\u0093\7\6\2\2\u0092\u0094\5\20\t\2\u0093\u0092\3\2\2\2\u0093\u0094"+
		"\3\2\2\2\u0094\u0095\3\2\2\2\u0095\u0098\7\7\2\2\u0096\u0097\7(\2\2\u0097"+
		"\u0099\5R*\2\u0098\u0096\3\2\2\2\u0098\u0099\3\2\2\2\u0099\u009a\3\2\2"+
		"\2\u009a\u009e\7\3\2\2\u009b\u009d\5\26\f\2\u009c\u009b\3\2\2\2\u009d"+
		"\u00a0\3\2\2\2\u009e\u009c\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a2\3\2"+
		"\2\2\u00a0\u009e\3\2\2\2\u00a1\u00a3\5\24\13\2\u00a2\u00a1\3\2\2\2\u00a2"+
		"\u00a3\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\u00a5\7\4\2\2\u00a5\r\3\2\2\2"+
		"\u00a6\u00ad\7/\2\2\u00a7\u00a9\7\6\2\2\u00a8\u00aa\5\22\n\2\u00a9\u00a8"+
		"\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ae\7\7\2\2\u00ac"+
		"\u00ae\7\5\2\2\u00ad\u00a7\3\2\2\2\u00ad\u00ac\3\2\2\2\u00ae\u00b0\3\2"+
		"\2\2\u00af\u00b1\7)\2\2\u00b0\u00af\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1"+
		"\17\3\2\2\2\u00b2\u00b3\7/\2\2\u00b3\u00b4\7(\2\2\u00b4\u00b5\5R*\2\u00b5"+
		"\u00bc\3\2\2\2\u00b6\u00b7\7\b\2\2\u00b7\u00b8\7/\2\2\u00b8\u00b9\7(\2"+
		"\2\u00b9\u00bb\5R*\2\u00ba\u00b6\3\2\2\2\u00bb\u00be\3\2\2\2\u00bc\u00ba"+
		"\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\21\3\2\2\2\u00be\u00bc\3\2\2\2\u00bf"+
		"\u00c4\5T+\2\u00c0\u00c1\7\b\2\2\u00c1\u00c3\5T+\2\u00c2\u00c0\3\2\2\2"+
		"\u00c3\u00c6\3\2\2\2\u00c4\u00c2\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\23"+
		"\3\2\2\2\u00c6\u00c4\3\2\2\2\u00c7\u00c8\7\16\2\2\u00c8\u00c9\5T+\2\u00c9"+
		"\u00ca\7)\2\2\u00ca\25\3\2\2\2\u00cb\u00d3\5L\'\2\u00cc\u00d3\5F$\2\u00cd"+
		"\u00d3\5J&\2\u00ce\u00d3\5N(\2\u00cf\u00d3\5\16\b\2\u00d0\u00d3\5\30\r"+
		"\2\u00d1\u00d3\5 \21\2\u00d2\u00cb\3\2\2\2\u00d2\u00cc\3\2\2\2\u00d2\u00cd"+
		"\3\2\2\2\u00d2\u00ce\3\2\2\2\u00d2\u00cf\3\2\2\2\u00d2\u00d0\3\2\2\2\u00d2"+
		"\u00d1\3\2\2\2\u00d3\27\3\2\2\2\u00d4\u00d6\5\32\16\2\u00d5\u00d7\5\36"+
		"\20\2\u00d6\u00d5\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7\31\3\2\2\2\u00d8\u00d9"+
		"\7\17\2\2\u00d9\u00da\7\6\2\2\u00da\u00db\5\"\22\2\u00db\u00dc\7\7\2\2"+
		"\u00dc\u00e0\7\3\2\2\u00dd\u00df\5\26\f\2\u00de\u00dd\3\2\2\2\u00df\u00e2"+
		"\3\2\2\2\u00e0\u00de\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1\u00e4\3\2\2\2\u00e2"+
		"\u00e0\3\2\2\2\u00e3\u00e5\5\24\13\2\u00e4\u00e3\3\2\2\2\u00e4\u00e5\3"+
		"\2\2\2\u00e5\u00e6\3\2\2\2\u00e6\u00e8\7\4\2\2\u00e7\u00e9\5\34\17\2\u00e8"+
		"\u00e7\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9\33\3\2\2\2\u00ea\u00eb\7\20\2"+
		"\2\u00eb\u00ec\5\32\16\2\u00ec\35\3\2\2\2\u00ed\u00ee\7\20\2\2\u00ee\u00f2"+
		"\7\3\2\2\u00ef\u00f1\5\26\f\2\u00f0\u00ef\3\2\2\2\u00f1\u00f4\3\2\2\2"+
		"\u00f2\u00f0\3\2\2\2\u00f2\u00f3\3\2\2\2\u00f3\u00f6\3\2\2\2\u00f4\u00f2"+
		"\3\2\2\2\u00f5\u00f7\5\24\13\2\u00f6\u00f5\3\2\2\2\u00f6\u00f7\3\2\2\2"+
		"\u00f7\u00f8\3\2\2\2\u00f8\u00f9\7\4\2\2\u00f9\37\3\2\2\2\u00fa\u00fb"+
		"\7\21\2\2\u00fb\u00fc\7\6\2\2\u00fc\u00fd\5\"\22\2\u00fd\u00fe\7\7\2\2"+
		"\u00fe\u0102\7\3\2\2\u00ff\u0101\5\26\f\2\u0100\u00ff\3\2\2\2\u0101\u0104"+
		"\3\2\2\2\u0102\u0100\3\2\2\2\u0102\u0103\3\2\2\2\u0103\u0105\3\2\2\2\u0104"+
		"\u0102\3\2\2\2\u0105\u0106\7\4\2\2\u0106!\3\2\2\2\u0107\u010d\5$\23\2"+
		"\u0108\u010d\5&\24\2\u0109\u010d\5(\25\2\u010a\u010d\5*\26\2\u010b\u010d"+
		"\5,\27\2\u010c\u0107\3\2\2\2\u010c\u0108\3\2\2\2\u010c\u0109\3\2\2\2\u010c"+
		"\u010a\3\2\2\2\u010c\u010b\3\2\2\2\u010d#\3\2\2\2\u010e\u010f\5.\30\2"+
		"\u010f\u0113\5> \2\u0110\u0112\5<\37\2\u0111\u0110\3\2\2\2\u0112\u0115"+
		"\3\2\2\2\u0113\u0111\3\2\2\2\u0113\u0114\3\2\2\2\u0114\u0116\3\2\2\2\u0115"+
		"\u0113\3\2\2\2\u0116\u0117\5$\23\2\u0117\u0132\3\2\2\2\u0118\u011a\5."+
		"\30\2\u0119\u011b\5<\37\2\u011a\u0119\3\2\2\2\u011b\u011c\3\2\2\2\u011c"+
		"\u011a\3\2\2\2\u011c\u011d\3\2\2\2\u011d\u011e\3\2\2\2\u011e\u011f\5$"+
		"\23\2\u011f\u0132\3\2\2\2\u0120\u0121\5.\30\2\u0121\u0122\5@!\2\u0122"+
		"\u0123\5$\23\2\u0123\u0132\3\2\2\2\u0124\u0125\5.\30\2\u0125\u0126\5B"+
		"\"\2\u0126\u0127\5$\23\2\u0127\u0132\3\2\2\2\u0128\u0129\7\6\2\2\u0129"+
		"\u012a\5$\23\2\u012a\u012e\7\7\2\2\u012b\u012c\58\35\2\u012c\u012d\5$"+
		"\23\2\u012d\u012f\3\2\2\2\u012e\u012b\3\2\2\2\u012e\u012f\3\2\2\2\u012f"+
		"\u0132\3\2\2\2\u0130\u0132\5.\30\2\u0131\u010e\3\2\2\2\u0131\u0118\3\2"+
		"\2\2\u0131\u0120\3\2\2\2\u0131\u0124\3\2\2\2\u0131\u0128\3\2\2\2\u0131"+
		"\u0130\3\2\2\2\u0132%\3\2\2\2\u0133\u0134\5\60\31\2\u0134\u0138\5> \2"+
		"\u0135\u0137\5<\37\2\u0136\u0135\3\2\2\2\u0137\u013a\3\2\2\2\u0138\u0136"+
		"\3\2\2\2\u0138\u0139\3\2\2\2\u0139\u013b\3\2\2\2\u013a\u0138\3\2\2\2\u013b"+
		"\u013c\5&\24\2\u013c\u0157\3\2\2\2\u013d\u013f\5\60\31\2\u013e\u0140\5"+
		"<\37\2\u013f\u013e\3\2\2\2\u0140\u0141\3\2\2\2\u0141\u013f\3\2\2\2\u0141"+
		"\u0142\3\2\2\2\u0142\u0143\3\2\2\2\u0143\u0144\5&\24\2\u0144\u0157\3\2"+
		"\2\2\u0145\u0146\5\60\31\2\u0146\u0147\5@!\2\u0147\u0148\5&\24\2\u0148"+
		"\u0157\3\2\2\2\u0149\u014a\5\60\31\2\u014a\u014b\5B\"\2\u014b\u014c\5"+
		"&\24\2\u014c\u0157\3\2\2\2\u014d\u014e\7\6\2\2\u014e\u014f\5&\24\2\u014f"+
		"\u0153\7\7\2\2\u0150\u0151\58\35\2\u0151\u0152\5&\24\2\u0152\u0154\3\2"+
		"\2\2\u0153\u0150\3\2\2\2\u0153\u0154\3\2\2\2\u0154\u0157\3\2\2\2\u0155"+
		"\u0157\5\60\31\2\u0156\u0133\3\2\2\2\u0156\u013d\3\2\2\2\u0156\u0145\3"+
		"\2\2\2\u0156\u0149\3\2\2\2\u0156\u014d\3\2\2\2\u0156\u0155\3\2\2\2\u0157"+
		"\'\3\2\2\2\u0158\u0159\5\62\32\2\u0159\u015a\5@!\2\u015a\u015b\5(\25\2"+
		"\u015b\u016a\3\2\2\2\u015c\u015d\5\62\32\2\u015d\u015e\5B\"\2\u015e\u015f"+
		"\5(\25\2\u015f\u016a\3\2\2\2\u0160\u0161\7\6\2\2\u0161\u0162\5(\25\2\u0162"+
		"\u0166\7\7\2\2\u0163\u0164\5:\36\2\u0164\u0165\5(\25\2\u0165\u0167\3\2"+
		"\2\2\u0166\u0163\3\2\2\2\u0166\u0167\3\2\2\2\u0167\u016a\3\2\2\2\u0168"+
		"\u016a\5\62\32\2\u0169\u0158\3\2\2\2\u0169\u015c\3\2\2\2\u0169\u0160\3"+
		"\2\2\2\u0169\u0168\3\2\2\2\u016a)\3\2\2\2\u016b\u016c\5\64\33\2\u016c"+
		"\u016d\5@!\2\u016d\u016e\5*\26\2\u016e\u017d\3\2\2\2\u016f\u0170\5\64"+
		"\33\2\u0170\u0171\5B\"\2\u0171\u0172\5*\26\2\u0172\u017d\3\2\2\2\u0173"+
		"\u0174\7\6\2\2\u0174\u0175\5*\26\2\u0175\u0179\7\7\2\2\u0176\u0177\5:"+
		"\36\2\u0177\u0178\5*\26\2\u0178\u017a\3\2\2\2\u0179\u0176\3\2\2\2\u0179"+
		"\u017a\3\2\2\2\u017a\u017d\3\2\2\2\u017b\u017d\5\64\33\2\u017c\u016b\3"+
		"\2\2\2\u017c\u016f\3\2\2\2\u017c\u0173\3\2\2\2\u017c\u017b\3\2\2\2\u017d"+
		"+\3\2\2\2\u017e\u017f\5\66\34\2\u017f\u0180\5@!\2\u0180\u0181\5,\27\2"+
		"\u0181\u0190\3\2\2\2\u0182\u0183\5\66\34\2\u0183\u0184\5B\"\2\u0184\u0185"+
		"\5,\27\2\u0185\u0190\3\2\2\2\u0186\u0187\7\6\2\2\u0187\u0188\5,\27\2\u0188"+
		"\u018c\7\7\2\2\u0189\u018a\5:\36\2\u018a\u018b\5,\27\2\u018b\u018d\3\2"+
		"\2\2\u018c\u0189\3\2\2\2\u018c\u018d\3\2\2\2\u018d\u0190\3\2\2\2\u018e"+
		"\u0190\5\66\34\2\u018f\u017e\3\2\2\2\u018f\u0182\3\2\2\2\u018f\u0186\3"+
		"\2\2\2\u018f\u018e\3\2\2\2\u0190-\3\2\2\2\u0191\u0193\5<\37\2\u0192\u0191"+
		"\3\2\2\2\u0193\u0196\3\2\2\2\u0194\u0192\3\2\2\2\u0194\u0195\3\2\2\2\u0195"+
		"\u0197\3\2\2\2\u0196\u0194\3\2\2\2\u0197\u019b\7*\2\2\u0198\u019b\5\16"+
		"\b\2\u0199\u019b\7/\2\2\u019a\u0194\3\2\2\2\u019a\u0198\3\2\2\2\u019a"+
		"\u0199\3\2\2\2\u019b/\3\2\2\2\u019c\u019e\5<\37\2\u019d\u019c\3\2\2\2"+
		"\u019e\u01a1\3\2\2\2\u019f\u019d\3\2\2\2\u019f\u01a0\3\2\2\2\u01a0\u01a2"+
		"\3\2\2\2\u01a1\u019f\3\2\2\2\u01a2\u01ad\7*\2\2\u01a3\u01a5\5<\37\2\u01a4"+
		"\u01a3\3\2\2\2\u01a5\u01a8\3\2\2\2\u01a6\u01a4\3\2\2\2\u01a6\u01a7\3\2"+
		"\2\2\u01a7\u01a9\3\2\2\2\u01a8\u01a6\3\2\2\2\u01a9\u01ad\7+\2\2\u01aa"+
		"\u01ad\5\16\b\2\u01ab\u01ad\7/\2\2\u01ac\u019f\3\2\2\2\u01ac\u01a6\3\2"+
		"\2\2\u01ac\u01aa\3\2\2\2\u01ac\u01ab\3\2\2\2\u01ad\61\3\2\2\2\u01ae\u01b2"+
		"\7,\2\2\u01af\u01b2\7/\2\2\u01b0\u01b2\5\16\b\2\u01b1\u01ae\3\2\2\2\u01b1"+
		"\u01af\3\2\2\2\u01b1\u01b0\3\2\2\2\u01b2\63\3\2\2\2\u01b3\u01b7\7-\2\2"+
		"\u01b4\u01b7\7/\2\2\u01b5\u01b7\5\16\b\2\u01b6\u01b3\3\2\2\2\u01b6\u01b4"+
		"\3\2\2\2\u01b6\u01b5\3\2\2\2\u01b7\65\3\2\2\2\u01b8\u01bc\5V,\2\u01b9"+
		"\u01bc\7/\2\2\u01ba\u01bc\5\16\b\2\u01bb\u01b8\3\2\2\2\u01bb\u01b9\3\2"+
		"\2\2\u01bb\u01ba\3\2\2\2\u01bc\67\3\2\2\2\u01bd\u01c2\5<\37\2\u01be\u01c2"+
		"\5> \2\u01bf\u01c2\5@!\2\u01c0\u01c2\5B\"\2\u01c1\u01bd\3\2\2\2\u01c1"+
		"\u01be\3\2\2\2\u01c1\u01bf\3\2\2\2\u01c1\u01c0\3\2\2\2\u01c29\3\2\2\2"+
		"\u01c3\u01c6\5@!\2\u01c4\u01c6\5B\"\2\u01c5\u01c3\3\2\2\2\u01c5\u01c4"+
		"\3\2\2\2\u01c6;\3\2\2\2\u01c7\u01c8\t\2\2\2\u01c8=\3\2\2\2\u01c9\u01ca"+
		"\t\3\2\2\u01ca?\3\2\2\2\u01cb\u01cc\t\4\2\2\u01ccA\3\2\2\2\u01cd\u01ce"+
		"\t\5\2\2\u01ceC\3\2\2\2\u01cf\u01d0\7\13\2\2\u01d0\u01d1\7/\2\2\u01d1"+
		"\u01d2\7(\2\2\u01d2\u01d3\5P)\2\u01d3\u01d4\7)\2\2\u01d4E\3\2\2\2\u01d5"+
		"\u01d6\7\f\2\2\u01d6\u01d7\7/\2\2\u01d7\u01d8\7(\2\2\u01d8\u01d9\5P)\2"+
		"\u01d9\u01da\7)\2\2\u01daG\3\2\2\2\u01db\u01dc\7\f\2\2\u01dc\u01dd\7/"+
		"\2\2\u01dd\u01de\7(\2\2\u01de\u01df\5R*\2\u01df\u01e0\7)\2\2\u01e0I\3"+
		"\2\2\2\u01e1\u01e2\7/\2\2\u01e2\u01e3\7\'\2\2\u01e3\u01e4\5T+\2\u01e4"+
		"\u01e5\7)\2\2\u01e5K\3\2\2\2\u01e6\u01e7\7/\2\2\u01e7\u01e8\7\'\2\2\u01e8"+
		"\u01e9\7\6\2\2\u01e9\u01ea\7/\2\2\u01ea\u01eb\58\35\2\u01eb\u01ec\7/\2"+
		"\2\u01ec\u01ed\7\7\2\2\u01ed\u01ee\7)\2\2\u01eeM\3\2\2\2\u01ef\u01f0\7"+
		"/\2\2\u01f0\u01f1\7\t\2\2\u01f1\u01f2\7/\2\2\u01f2\u01f3\7\'\2\2\u01f3"+
		"\u01f4\5T+\2\u01f4\u01f5\7)\2\2\u01f5O\3\2\2\2\u01f6\u01f7\7\22\2\2\u01f7"+
		"\u01fa\7\'\2\2\u01f8\u01fb\7*\2\2\u01f9\u01fb\5$\23\2\u01fa\u01f8\3\2"+
		"\2\2\u01fa\u01f9\3\2\2\2\u01fb\u0214\3\2\2\2\u01fc\u01fd\7\23\2\2\u01fd"+
		"\u0200\7\'\2\2\u01fe\u0201\7+\2\2\u01ff\u0201\5&\24\2\u0200\u01fe\3\2"+
		"\2\2\u0200\u01ff\3\2\2\2\u0201\u0214\3\2\2\2\u0202\u0203\7\24\2\2\u0203"+
		"\u0204\7\'\2\2\u0204\u0214\7,\2\2\u0205\u0206\7\25\2\2\u0206\u0207\7\'"+
		"\2\2\u0207\u0214\7-\2\2\u0208\u0209\7\26\2\2\u0209\u020c\7\'\2\2\u020a"+
		"\u020d\5V,\2\u020b\u020d\5,\27\2\u020c\u020a\3\2\2\2\u020c\u020b\3\2\2"+
		"\2\u020d\u0214\3\2\2\2\u020e\u020f\7.\2\2\u020f\u0210\7\'\2\2\u0210\u0214"+
		"\7/\2\2\u0211\u0214\7/\2\2\u0212\u0214\5\16\b\2\u0213\u01f6\3\2\2\2\u0213"+
		"\u01fc\3\2\2\2\u0213\u0202\3\2\2\2\u0213\u0205\3\2\2\2\u0213\u0208\3\2"+
		"\2\2\u0213\u020e\3\2\2\2\u0213\u0211\3\2\2\2\u0213\u0212\3\2\2\2\u0214"+
		"Q\3\2\2\2\u0215\u021c\7\22\2\2\u0216\u021c\7\23\2\2\u0217\u021c\7\24\2"+
		"\2\u0218\u021c\7\25\2\2\u0219\u021c\7\26\2\2\u021a\u021c\7.\2\2\u021b"+
		"\u0215\3\2\2\2\u021b\u0216\3\2\2\2\u021b\u0217\3\2\2\2\u021b\u0218\3\2"+
		"\2\2\u021b\u0219\3\2\2\2\u021b\u021a\3\2\2\2\u021cS\3\2\2\2\u021d\u0227"+
		"\7*\2\2\u021e\u0227\7+\2\2\u021f\u0227\7,\2\2\u0220\u0227\7-\2\2\u0221"+
		"\u0227\7\30\2\2\u0222\u0227\7\31\2\2\u0223\u0227\7/\2\2\u0224\u0227\5"+
		"\16\b\2\u0225\u0227\5\"\22\2\u0226\u021d\3\2\2\2\u0226\u021e\3\2\2\2\u0226"+
		"\u021f\3\2\2\2\u0226\u0220\3\2\2\2\u0226\u0221\3\2\2\2\u0226\u0222\3\2"+
		"\2\2\u0226\u0223\3\2\2\2\u0226\u0224\3\2\2\2\u0226\u0225\3\2\2\2\u0227"+
		"U\3\2\2\2\u0228\u022b\7\30\2\2\u0229\u022b\7\31\2\2\u022a\u0228\3\2\2"+
		"\2\u022a\u0229\3\2\2\2\u022bW\3\2\2\29\\bhq~\u008a\u0093\u0098\u009e\u00a2"+
		"\u00a9\u00ad\u00b0\u00bc\u00c4\u00d2\u00d6\u00e0\u00e4\u00e8\u00f2\u00f6"+
		"\u0102\u010c\u0113\u011c\u012e\u0131\u0138\u0141\u0153\u0156\u0166\u0169"+
		"\u0179\u017c\u018c\u018f\u0194\u019a\u019f\u01a6\u01ac\u01b1\u01b6\u01bb"+
		"\u01c1\u01c5\u01fa\u0200\u020c\u0213\u021b\u0226\u022a";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}