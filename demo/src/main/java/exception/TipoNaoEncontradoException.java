package exception;

public class TipoNaoEncontradoException extends RuntimeException {

    public TipoNaoEncontradoException( String mensagem ){
        super(mensagem);
    }

}
