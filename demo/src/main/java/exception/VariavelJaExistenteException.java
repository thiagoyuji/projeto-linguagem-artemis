package exception;

public class VariavelJaExistenteException extends RuntimeException {

    public VariavelJaExistenteException(String mensagem ){
        super(mensagem);
    }

}

