package exception;

public class BibliotecaJaImportadaException extends RuntimeException {

    public BibliotecaJaImportadaException( String mensagem ){
        super(mensagem);
    }
}
