package programa.modelo.variavel;

public class Variavel {

    private String nome;
    private TipoVariavel tipoVariavel;
    private String valor;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoVariavel getTipoVariavel() {
        return tipoVariavel;
    }

    public void setTipoVariavel(TipoVariavel tipoVariavel) { this.tipoVariavel = tipoVariavel; }

    public void setValor(String valor) { this.valor = valor; }

    public String getValor() { return this.valor; }
}
