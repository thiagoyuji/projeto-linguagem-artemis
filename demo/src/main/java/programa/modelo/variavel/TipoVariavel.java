package programa.modelo.variavel;


import exception.TipoNaoEncontradoException;

import java.util.Arrays;

public enum TipoVariavel {

    INT("i32", "int"),
    FLOAT("f64", "float");

    private String valor;
    private String valorArtemis;

    TipoVariavel( String valor, String valorArtemis ) { this.valor = valor; this.valorArtemis = valorArtemis; }

    public String getValor() { return this.valor; }

    public static  TipoVariavel construirTipoVariavel( String tipo ) {
        return Arrays.stream( TipoVariavel.values() )
                .filter( tipoAtual -> tipoAtual.valorArtemis.equals(tipo))
                .findFirst()
                .orElseThrow( () -> new TipoNaoEncontradoException(String.format("Error - Tipo [%s] nao Encontrado",tipo)));
    }

}
