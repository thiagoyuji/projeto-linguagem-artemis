package programa.instrucao.variavel;


import programa.instrucao.Instrucao;
import programa.modelo.variavel.TipoVariavel;
import programa.modelo.variavel.Variavel;

public class DeclaracaoVariavel implements Instrucao {

    private Variavel variavel;

    public DeclaracaoVariavel(Variavel var) {
        this.variavel = var;
    }

    public static DeclaracaoVariavel declararVariavel(String nome, String tipo) {
        Variavel var = new Variavel();
        var.setNome(nome);
        var.setTipoVariavel(TipoVariavel.construirTipoVariavel(tipo));
        return new DeclaracaoVariavel(var);
    }

    @Override
    public String obterInstrucao() {
        return gerarVariavelLocal();
    }

    private String gerarVariavelLocal() {
        return String.format("\n(local $%s %s)", variavel.getNome(), variavel.getTipoVariavel().getValor());
    }

    public Variavel getVariavel() {
        return this.variavel;
    }
}
