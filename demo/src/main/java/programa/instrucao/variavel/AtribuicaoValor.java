package programa.instrucao.variavel;

import programa.instrucao.Instrucao;
import programa.modelo.variavel.Variavel;

public class AtribuicaoValor implements Instrucao {

    private Variavel variavel;

    public AtribuicaoValor(Variavel variavel) {
        this.variavel = variavel;
    }

    public void atribuirValor(String valor) {
        this.variavel.setValor(valor);
    }

    @Override
    public String obterInstrucao() {
        // Obs: No momento só aceita atribuicao de constantes...
        return String.format("\n(local.set $%s (%s.const %s))",variavel.getNome(), variavel.getTipoVariavel().getValor(),variavel.getValor());
    }

    public Variavel getVariavel() {
        return this.variavel;
    }
}
