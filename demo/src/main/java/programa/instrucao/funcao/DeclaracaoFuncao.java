package programa.instrucao.funcao;

import exception.VariavelJaExistenteException;
import exception.VariavelNaoExisteException;
import programa.instrucao.Instrucao;
import programa.instrucao.operacao.Operacao;
import programa.instrucao.variavel.AtribuicaoValor;
import programa.instrucao.variavel.DeclaracaoVariavel;
import programa.modelo.variavel.Variavel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class DeclaracaoFuncao implements Instrucao {

    private String nome;
    private LinkedList<Instrucao> instrucoes;
    private LinkedList<AtribuicaoValor> atribuicoes;
    private Map<String, Variavel> variaveisLocais;

    public DeclaracaoFuncao(String nome) {
        this.nome = nome;
        instrucoes = new LinkedList<>();
        variaveisLocais = new HashMap();
        atribuicoes = new LinkedList<>();
    }

    @Override
    public String obterInstrucao() {
        return gerarFuncao();
    }

    public void inserirInstrucao(Instrucao instrucao) {
        boolean toAdd = true;
        if(instrucao instanceof DeclaracaoVariavel) {
            inserirVariavel(instrucao);
        } else if(instrucao instanceof AtribuicaoValor) {
            toAdd = false;
            validarAtribuicao(instrucao);
        } else {
            addAllAtribuicoes();
            if(instrucao instanceof Operacao) {
                criarOperacao(instrucao);
            }
        }

        if(toAdd)
            this.instrucoes.add(instrucao);
    }

    private void addAllAtribuicoes() {
        Instrucao instrucao = atribuicoes.poll();
        while (instrucao != null) {
            this.instrucoes.add(instrucao);
            instrucao = atribuicoes.poll();
        }
    }

    private void criarOperacao(Instrucao instrucao) {
        Operacao opt = (Operacao) instrucao;
        opt.montarOperacao(this.variaveisLocais);
    }

    private void inserirVariavel(Instrucao instrucao) {
        Variavel var = ((DeclaracaoVariavel) instrucao).getVariavel();
        if(this.variaveisLocais.containsKey(var.getNome())) {
            throw new VariavelJaExistenteException(String.format("Error - Variavel Global [%s] ja existe", var.getNome()));
        } else {
            this.variaveisLocais.put(var.getNome(), var);
        }
    }

    private void validarAtribuicao(Instrucao instrucao) {
        Variavel var = ((AtribuicaoValor) instrucao).getVariavel();
        if(!this.variaveisLocais.containsKey(var.getNome())) {
            throw new VariavelNaoExisteException(String.format("Variavel %s não existe no escopo.",var.getNome()));
        }
        this.atribuicoes.add((AtribuicaoValor) instrucao);
    }

    private String gerarFuncao() {
        StringBuilder func = new StringBuilder();

        func.append(gerarCabecalho());

        Instrucao instrucao = instrucoes.poll();
        while (instrucao != null) {
            func.append(instrucao.obterInstrucao());
            instrucao = instrucoes.poll();
        }

        func.append("\n)");
        return func.toString();
    }

    private String gerarCabecalho() {
        return String.format("\n(func $%s (export \"%s\") ", nome,nome);
    }

}
