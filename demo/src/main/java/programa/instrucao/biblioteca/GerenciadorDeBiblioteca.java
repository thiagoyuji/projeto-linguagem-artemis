package programa.instrucao.biblioteca;

import programa.instrucao.Instrucao;
import programa.instrucao.biblioteca.io.IOBiblioteca;

import java.util.HashMap;
import java.util.Map;

public class GerenciadorDeBiblioteca {
    private Map<String, Instrucao> libs;

    public GerenciadorDeBiblioteca() {
        this.libs = new HashMap<>();
        IOBiblioteca ioLib = new IOBiblioteca();
        libs.put(ioLib.getNome(), ioLib);
    }

    public Instrucao obterLib(String nomeLib) {
        return libs.get(nomeLib);
    }
}
