package programa.instrucao.biblioteca.io;

import programa.instrucao.Instrucao;

public class IOBiblioteca implements Instrucao {
    private String nome;

    public IOBiblioteca() {
        this.nome = "io_library";
    }
    @Override
    public String obterInstrucao() {
        return lib();
    }

    private String lib()  {
        return "\n(func $print (import \"imports\" \"imported_func\") (param i32) )";
    }

    public String getNome(){ return nome; }
}
