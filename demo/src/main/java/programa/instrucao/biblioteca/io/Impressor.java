package programa.instrucao.biblioteca.io;

import programa.instrucao.Instrucao;

public class Impressor implements Instrucao {

    private String valorImprimido;

    public Impressor(String valorImprimido){
        this.valorImprimido = valorImprimido;
    }

    @Override
    public String obterInstrucao() {
        return imprimirVariavelInt(valorImprimido);
    }

    private String imprimirVariavelInt(String nomeVariavel) {
        return String.format("\n(call $print (local.get $%s))", nomeVariavel);
    }
}
