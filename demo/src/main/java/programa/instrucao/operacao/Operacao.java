package programa.instrucao.operacao;

import programa.instrucao.Instrucao;
import programa.modelo.operacao.TipoOperacao;
import programa.modelo.variavel.Variavel;

import java.util.Map;

public class Operacao implements Instrucao {

    private Instrucao operacao;

    private String varDestino;
    private String opt1;
    private String opt2;
    private String operando;

    public Operacao(String varDestino, String opt1, String opt2, String operando) {
        this.varDestino = varDestino;
        this.opt1 = opt1;
        this.opt2 = opt2;
        this.operando = operando;
    }

    @Override
    public String obterInstrucao() {
        return this.operacao.obterInstrucao();
    }

    public void montarOperacao(Map<String, Variavel> variaveisLocais) {
        Variavel varDest =  variaveisLocais.get(varDestino);
        Variavel operando1 =  variaveisLocais.get(opt1);
        Variavel operando2 =  variaveisLocais.get(opt2);
        TipoOperacao opt = TipoOperacao.construirTipoOperacao(operando);

        this.operacao = new OperacaoAritmetica(varDest,operando1,operando2,opt);
    }
}
